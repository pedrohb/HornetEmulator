﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.HelperTool;
using Plus.HabboHotel.Guides;

namespace Plus.Communication.Packets.Incoming.HelperTool
{
    class HelperSessioChatTypingMessageEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            bool typing = Packet.PopBoolean();

            GuideTour tour = MithServer.GetGame().GetGuideManager().getGuideTourByHabbo(Session.GetHabbo());

            if (tour != null)
            {
                if (tour.getHelper() ==Session.GetHabbo())
                {
                    tour.getNoob().GetClient().SendMessage(new HelperSessionChatIsTypingMessageComposer(typing));
                }
                else
                {
                    tour.getHelper().GetClient().SendMessage(new HelperSessionChatIsTypingMessageComposer(typing));
                }
            }


        }
    }
}