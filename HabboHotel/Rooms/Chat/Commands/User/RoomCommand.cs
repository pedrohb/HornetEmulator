﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;



using Plus.Communication.Packets.Outgoing.Rooms.Engine;

using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class RoomCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_room"; }
        }

        public string Parameters
        {
            get { return "list/push/pull/enable/respect"; }
        }

        public string Description
        {
            get { return "Te permite habilitar o deshabilitar los comandos básicos de la sala."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Vaya, debes elegir una opción de sala para deshabilitarla.");
                return;
            }

            if (!Room.CheckRights(Session, true))
            {
                return;
            }

            string Option = Params[1];
            switch (Option)
            {
                case "list":
                {
                    StringBuilder List = new StringBuilder("");
                    List.AppendLine("Lista del comando Room");
                    List.AppendLine("-------------------------");
                    List.AppendLine("Pet Morphs: " + (Room.PetMorphsAllowed == true ? "activado" : "desactivado"));
                    List.AppendLine("Pull: " + (Room.PullEnabled == true ? "activado" : "desactivado"));
                    List.AppendLine("Push: " + (Room.PushEnabled == true ? "activado" : "desactivado"));
                    List.AppendLine("Super Pull: " + (Room.SPullEnabled == true ? "activado" : "desactivado"));
                    List.AppendLine("Super Push: " + (Room.SPushEnabled == true ? "activado" : "desactivado"));
                    List.AppendLine("Respect: " + (Room.RespectNotificationsEnabled == true ? "activado" : "desactivado"));
                    List.AppendLine("Enables: " + (Room.EnablesEnabled == true ? "activado" : "desactivado"));
                    Session.SendNotification(List.ToString());
                    break;
                }

                case "push":
                    {
                        Room.PushEnabled = !Room.PushEnabled;
                        using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `rooms` SET `push_enabled` = @PushEnabled WHERE `id` = '" + Room.Id +"' LIMIT 1");
                            dbClient.AddParameter("PushEnabled", MithServer.BoolToEnum(Room.PushEnabled));
                            dbClient.RunQuery();
                        }

                        Session.SendWhisper("El comando Push ahora esta " + (Room.PushEnabled == true ? "activado!" : "desactivado!"));
                        break;
                    }

                case "spush":
                    {
                        Room.SPushEnabled = !Room.SPushEnabled;
                        using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `rooms` SET `spush_enabled` = @PushEnabled WHERE `id` = '" + Room.Id + "' LIMIT 1");
                            dbClient.AddParameter("PushEnabled", MithServer.BoolToEnum(Room.SPushEnabled));
                            dbClient.RunQuery();
                        }

                        Session.SendWhisper("El comando Super Push ahora esta " + (Room.SPushEnabled == true ? "activado!" : "desactivado!"));
                        break;
                    }

                case "spull":
                    {
                        Room.SPullEnabled = !Room.SPullEnabled;
                        using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `rooms` SET `spull_enabled` = @PullEnabled WHERE `id` = '" + Room.Id + "' LIMIT 1");
                            dbClient.AddParameter("PullEnabled", MithServer.BoolToEnum(Room.SPullEnabled));
                            dbClient.RunQuery();
                        }

                        Session.SendWhisper("El comando Super Pull ahora esta " + (Room.SPullEnabled == true ? "activado!" : "desactivado!"));
                        break;
                    }

                case "pull":
                    {
                        Room.PullEnabled = !Room.PullEnabled;
                        using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `rooms` SET `pull_enabled` = @PullEnabled WHERE `id` = '" + Room.Id + "' LIMIT 1");
                            dbClient.AddParameter("PullEnabled", MithServer.BoolToEnum(Room.PullEnabled));
                            dbClient.RunQuery();
                        }

                        Session.SendWhisper("El comando Pull ahora esta " + (Room.PullEnabled == true ? "activado!" : "desactivado!"));
                        break;
                    }

                case "enable":
                case "enables":
                    {
                        Room.EnablesEnabled = !Room.EnablesEnabled;
                        using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `rooms` SET `enables_enabled` = @EnablesEnabled WHERE `id` = '" + Room.Id + "' LIMIT 1");
                            dbClient.AddParameter("EnablesEnabled", MithServer.BoolToEnum(Room.EnablesEnabled));
                            dbClient.RunQuery();
                        }

                        Session.SendWhisper("Los efectos ahora estan " + (Room.EnablesEnabled == true ? "activado!" : "desactivado!"));
                        break;
                    }

                case "respect":
                case "respeto":
                case "respetos":
                    {
                        Room.RespectNotificationsEnabled = !Room.RespectNotificationsEnabled;
                        using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `rooms` SET `respect_notifications_enabled` = @RespectNotificationsEnabled WHERE `id` = '" + Room.Id + "' LIMIT 1");
                            dbClient.AddParameter("RespectNotificationsEnabled", MithServer.BoolToEnum(Room.RespectNotificationsEnabled));
                            dbClient.RunQuery();
                        }

                        Session.SendWhisper("Respect notifications mode set to " + (Room.RespectNotificationsEnabled == true ? "activado!" : "desactivado!"));
                        break;
                    }

                case "pets":
                case "morphs":
                case "mascotas":
                    {
                        Room.PetMorphsAllowed = !Room.PetMorphsAllowed;
                        using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `rooms` SET `pet_morphs_allowed` = @PetMorphsAllowed WHERE `id` = '" + Room.Id + "' LIMIT 1");
                            dbClient.AddParameter("PetMorphsAllowed", MithServer.BoolToEnum(Room.PetMorphsAllowed));
                            dbClient.RunQuery();
                        }

                        Session.SendWhisper("Human pet morphs notifications mode set to " + (Room.PetMorphsAllowed == true ? "activado!" : "desactivado!"));
                        
                        if (!Room.PetMorphsAllowed)
                        {
                            foreach (RoomUser User in Room.GetRoomUserManager().GetRoomUsers())
                            {
                                if (User == null || User.GetClient() == null || User.GetClient().GetHabbo() == null)
                                    continue;

                                User.GetClient().SendWhisper("The room owner has disabled the ability to use a pet morph in this room.");
                                if (User.GetClient().GetHabbo().PetId > 0)
                                {
                                    //Tell the user what is going on.
                                    User.GetClient().SendWhisper("Oops, the room owner has just disabled pet-morphs, un-morphing you.");
                                    
                                    //Change the users Pet Id.
                                    User.GetClient().GetHabbo().PetId = 0;

                                    //Quickly remove the old user instance.
                                    Room.SendMessage(new UserRemoveComposer(User.VirtualId));

                                    //Add the new one, they won't even notice a thing!!11 8-)
                                    Room.SendMessage(new UsersComposer(User));
                                }
                            }
                        }
                        break;
                    }
            }
        }
    }
}
