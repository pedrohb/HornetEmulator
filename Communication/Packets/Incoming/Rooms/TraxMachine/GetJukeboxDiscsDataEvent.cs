﻿using Plus.Communication.Packets.Outgoing.Sound;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Rooms.TraxMachine;
using System;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.TraxMachine;
using Plus.Database.Interfaces;
using System.Data;

namespace Plus.Communication.Packets.Incoming.TraxMachine
{
    internal class GetJukeboxDiscsDataEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            int num = Packet.PopInt();
            List<TraxMusicData> list = new List<TraxMusicData>();
            while (checked(num--) > 0)
            {
                int id = Packet.PopInt();
                TraxMusicData music = TraxSoundManager.GetMusic(id);
                bool flag = music != null;
                if (flag)
                {
                    list.Add(music);
                }
            }
            bool flag2 = Session.GetHabbo().CurrentRoom != null;
            if (flag2)
            {
                Session.SendMessage(new SetJukeboxSongMusicDataComposer(list));
            }
            //int id = Packet.PopInt();
            //DataRow Row = null;
            //using (IQueryAdapter queryReactor = MithServer.GetDatabaseManager().GetQueryReactor())
            //{
            //    queryReactor.RunQuery("SELECT * FROM jukebox_songs_data WHERE id = '"+id+"' LIMIT 1");
            //    Row = queryReactor.getRow();
            //}
            //string codename = Row[1].ToString();
            //string name = Row[2].ToString();
            //string artist = Row[3].ToString();
            //string data = Row[4].ToString();
            //int length = Convert.ToInt32(Row[5]);

            //Session.SendMessage(new SetJukeboxSongMusicDataComposer(id, codename, name,data,length,artist));
        }
    }
}
