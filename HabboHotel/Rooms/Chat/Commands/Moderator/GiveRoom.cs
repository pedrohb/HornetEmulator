﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Rooms;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class GiveRoom : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_give_room"; }
        }

        public string Parameters
        {
            get { return "%cantidad%"; }
        }

        public string Description
        {
            get { return "Give a badge to the entire room!"; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Please enter the name of the badge you'd like to give to the room.");
                return;
            }
            int Amount;
            if (int.TryParse(Params[1], out Amount))

                foreach (RoomUser RoomUser in Room.GetRoomUserManager().GetRoomUsers())
            {
                if (RoomUser == null || RoomUser.GetClient() == null || Session.GetHabbo().Id == RoomUser.UserId)
                    continue;
                RoomUser.GetClient().GetHabbo().Credits  += Amount;
                RoomUser.GetClient().SendMessage(new CreditBalanceComposer(RoomUser.GetClient().GetHabbo().Credits));
            }
    }
}
}
  