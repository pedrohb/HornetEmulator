﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.HelperTool;


namespace Plus.Communication.Packets.Incoming.HelperTool
{
    class HandleHelperToolMessageEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {

            bool OnService = Packet.PopBoolean();

            if (OnService)
            {
                bool tourRequests = Packet.PopBoolean();
                bool helperRequests = Packet.PopBoolean();
                bool bullyReports = Packet.PopBoolean();
                if (helperRequests)
                {
                    MithServer.GetGame().GetGuideManager().setOnGuide(Session.GetHabbo(), OnService);
                }

                if (bullyReports)
                {
                    //SetOnGuardian
                }

                Session.SendMessage(new HandleHelperToolMessageComposer(OnService));
            }
            else
            {
                //SetOnGuardian
                MithServer.GetGame().GetGuideManager().setOnGuide(Session.GetHabbo(), OnService);
                Session.SendMessage(new HandleHelperToolMessageComposer(OnService));
            }
        }
    }
}