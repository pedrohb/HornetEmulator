﻿#region

using System.Collections.Generic;
using System.Data;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Items;
using Plus.Messages;

using Plus.Database.Interfaces;
using Plus.Communication.Packets.Outgoing;
using log4net;
using System;
using Plus.Communication.Packets.Outgoing.Talents;

#endregion

namespace Plus.HabboHotel.Talentos
{

   
    public class TalentManager
    {
        private static readonly ILog log = LogManager.GetLogger("Plus.HabboHotel.Talentos");

        /// <summary>
        /// The talents
        /// </summary>
        internal Dictionary<int, TalentDefinition> Talents;

        /// <summary>
        /// Initializes a new instance of the <see cref="TalentManager"/> class.
        /// </summary>
        public TalentManager()
        {
            Talents = new Dictionary<int, TalentDefinition>();
            this.Initialize();
        }

        /// <summary>
        /// Initializes the specified database client.
        /// </summary>
        /// <param name="dbClient">The database client.</param>
        public void Initialize()
        {
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT * FROM achievements_talents ORDER BY `order_num` ASC");
            DataTable table = dbClient.getTable();
                if (table != null)
                {
                    foreach (DataRow dataRow in table.Rows)
                    {
                        var talent = new TalentDefinition((Convert.ToInt32(dataRow["id"])), Convert.ToString(dataRow["type"]), Convert.ToInt32(dataRow["parent_category"]), Convert.ToInt32(dataRow["level"]), Convert.ToString(dataRow["achievement_group"]), Convert.ToInt32(dataRow["achievement_level"]), Convert.ToString(dataRow["prize"]), Convert.ToInt32(dataRow["prize_baseitem"]));
                        Talents.Add(talent.Id, talent);


                    }
                }

            }
        }

        /// <summary>
        /// Gets the talent.
        /// </summary>
        /// <param name="TalentId">The talent identifier.</param>
        /// <returns>Talent.</returns>
        internal TalentDefinition GetTalent(int TalentId)
        {
            return Talents[TalentId];
        }

        /// <summary>
        /// Levels the is completed.
        /// </summary>
        /// <param name="Session">The session.</param>
        /// <param name="TrackType">Type of the track.</param>
        /// <param name="TalentLevel">The talent level.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        internal bool LevelIsCompleted(GameClient Session, string TrackType, int TalentLevel)
        {
            foreach (TalentDefinition current in GetTalents(TrackType, TalentLevel))
            {
                if (Session.GetHabbo().GetAchievementData(current.AchievementGroup) != null && Session.GetHabbo().GetAchievementData(current.AchievementGroup).Level >= current.AchievementLevel)
                    return false;
            }
   
            return true;
        }

        /// <summary>
        /// Completes the user talent.
        /// </summary>
        /// <param name="Session">The session.</param>
        /// <param name="Talent">The talent.</param>
        public void CompleteUserTalent(GameClient Session, TalentDefinition Talent)
        {
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().CurrentTalentLevel < Talent.Level || Session.GetHabbo().Talents.ContainsKey(Talent.Id))
                return;
            if (!LevelIsCompleted(Session, Talent.Type, Talent.Level))
                return;
            if (!string.IsNullOrEmpty(Talent.Prize) && Talent.PrizeBaseItem > 0u)
            {
               /*REGALOS*/

            }

            var value = new UserTalent(Talent.Id, 1);
            Session.GetHabbo().Talents.Add(Talent.Id, value);

          

            using (IQueryAdapter queryReactor = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunQuery(string.Concat("REPLACE INTO users_talents VALUES (", Session.GetHabbo().Id, ", ", Talent.Id, ", ", 1, ");"));
            }


            
            Session.SendMessage(new TalentLevelUpComposer(Talent));
        

            if (Talent.Type == "citizenship")
            {
                if (Talent.Level == 3)
                    MithServer.GetGame().GetAchievementManager().ProgressAchievement(Session, "ACH_Citizenship", 1);
                else if (Talent.Level == 4)
                {
                    using (IQueryAdapter queryReactor = MithServer.GetDatabaseManager().GetQueryReactor())
                    {
                        queryReactor.RunQuery(string.Concat(new object[]
                        {
                            "UPDATE users SET talentstatus = 'helper' WHERE id = ",
                            Session.GetHabbo().Id,
                            ";"
                        }));
                    }
                }
            }
        }

        /// <summary>
        /// Tries the get talent.
        /// </summary>
        /// <param name="AchGroup">The ach group.</param>
        /// <param name="talent">The talent.</param>
        /// <returns><c>true</c> if XXXX, <c>false</c> otherwise.</returns>
        internal bool TryGetTalent(string AchGroup, out TalentDefinition talent)
        {
            foreach (TalentDefinition current in Talents.Values)
            {
                if (current.AchievementGroup == AchGroup)
                {
                    talent = current;
                    return true;
                }
            }
            talent = null;
            return false;
        }

        /// <summary>
        /// Gets all talents.
        /// </summary>
        /// <returns>Dictionary&lt;System.Int32, Talent&gt;.</returns>
        internal Dictionary<int, TalentDefinition> GetAllTalents()
        {
            return Talents;
        }

        /// <summary>
        /// Gets the talents.
        /// </summary>
        /// <param name="TrackType">Type of the track.</param>
        /// <param name="ParentCategory">The parent category.</param>
        /// <returns>List&lt;Talent&gt;.</returns>
        internal List<TalentDefinition> GetTalents(string TrackType, int ParentCategory)
        {
            var list = new List<TalentDefinition>();
            foreach (TalentDefinition current in Talents.Values)
            {
                if (current.Type == TrackType && current.ParentCategory == ParentCategory)
                {
                    list.Add(current);
                }
            }
            return list;
        }
    }
}