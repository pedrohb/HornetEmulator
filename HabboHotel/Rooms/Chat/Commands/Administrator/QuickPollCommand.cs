﻿using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Polls;
using Plus.HabboHotel.Rooms;
using Plus.Database.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Plus.HabboHotel.GameClients;
using System.Threading.Tasks;
using System.Threading;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Administrator
{
    class QuickPollCommand : IChatCommand
    {

        public string PermissionRequired
        {
            get { return "command_quickpoll"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Envia una encuesta rapida (Pregunta)"; }
        }
        private List<string> encuestas;

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)

        {

            string question = CommandManager.MergeParams(Params, 1);
            if (string.IsNullOrEmpty(Session.GetHabbo().CurrentRoom.poolQuestion))
            {
                Session.GetHabbo().CurrentRoom.startQuestion(question);
                Task t = Task.Factory.StartNew(() => TaskStopQuestion(Session.GetHabbo().CurrentRoom));
            }
            else
            {
                Session.SendWhisper("Esta sala tiene una pregunta activa, debes esperar a que se termine para iniciar otra.");
                return;
            }


        }
        public void TaskStopQuestion(Room room)
        {
            Thread.Sleep(10000);
            room.endQuestion();
        }
    }
}

