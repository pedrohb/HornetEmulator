﻿using Plus.Communication.Packets.Outgoing.Sound;
using Plus.Communication.Packets.Outgoing.TraxMachine;
using Plus.HabboHotel.GameClients;
using System;

namespace Plus.Communication.Packets.Incoming.TraxMachine
{
    internal class LoadJukeboxDiscsEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            bool flag = Session.GetHabbo().CurrentRoom != null;
            if (flag)
            {
                Session.SendMessage(new LoadJukeboxUserMusicItemsComposer(Session.GetHabbo().CurrentRoom));
            }
        }
    }
}
