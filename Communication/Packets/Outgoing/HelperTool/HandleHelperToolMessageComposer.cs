﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.HelperTool
{
    class HandleHelperToolMessageComposer : ServerPacket
    {
        public HandleHelperToolMessageComposer(bool OnService)
            : base(ServerPacketHeader.HandleHelperToolMessageComposer)
        {
            base.WriteBoolean(OnService); //OnService
            base.WriteInteger(0); //Guides On Duty
            base.WriteInteger(MithServer.GetGame().GetGuideManager().getGuidesCount());//Helpers On Duty
            base.WriteInteger(0);//Guardians On Duty

        }
    }
}