﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plus.HabboHotel.Guides;
using Plus.HabboHotel.Users;
using System.Collections.Concurrent;

namespace Plus.HabboHotel.Guides
{
    public class GuideTour
    {
        // private ConcurrentDictionary<int, Habbo> mrd = new ConcurrentDictionary<int, Habbo>(); guia
        public int checkSum = 0;

        private Habbo helper;
        private int startTime;
        private int endTime;
        public bool beinghelped = false;
        public bool helping = false;
        public bool activated = false;
        private bool ended;
        private GuideRecommendStatus wouldRecommend = GuideRecommendStatus.UNKNOWN;

        private Habbo noob;
        private string helpRequest;
        private List<GuideChatMessage> sendMessages = new List<GuideChatMessage>();
        private List<int> declinedHelpers = new List<int>();

        public GuideTour(Habbo noob, String helpRequest)
        {
            this.noob = noob;
            this.helpRequest = helpRequest;

        }

        public void finish()
        {
            //TODO Insert recommendation.
            //TODO Query messages.
        }
        public Habbo getNoob()
        {
            return this.noob;
        }

        public String getHelpRequest()
        {
            return this.helpRequest;
        }

        public Habbo getHelper()
        {
            return this.helper;
        }

        public void setHelper(Habbo helper)
        {
            this.helper = helper;
        }

        public void addMessage(GuideChatMessage message)
        {
            this.sendMessages.Add(message);
        }

        public GuideRecommendStatus getWouldRecommend()
        {
            return this.wouldRecommend;
        }

        public void setWouldRecommend(GuideRecommendStatus wouldRecommend)
        {
            this.wouldRecommend = wouldRecommend;

            if (this.wouldRecommend == GuideRecommendStatus.YES)
            {
                //Query Vote positivo
            }
            else if (this.wouldRecommend == GuideRecommendStatus.NO)
            {
                //Query Vote negativo
            }
        }

        public void addDeclinedHelper(int userId)
        {
            this.declinedHelpers.Add(userId);
        }

        public bool hasDeclined(int userId)
        {
            return this.declinedHelpers.Contains(userId);
        }

        public void end()
        {
            this.ended = true;
            this.endTime = Convert.ToInt32(MithServer.GetUnixTimestamp());
        }

        public bool isEnded()
        {
            return this.ended;
        }

        public int getStartTime()
        {
            return this.startTime;
        }

        public void setStartTime(int startTime)
        {
            this.startTime = startTime;
        }

        public int getEndTime()
        {
            return this.endTime;
        }



    }
}
