﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.HelperTool;
using Plus.HabboHotel.Guides;

namespace Plus.Communication.Packets.Incoming.HelperTool
{
    class CloseHelperChatSessionMessageEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            GuideTour tour = MithServer.GetGame().GetGuideManager().getGuideTourByHabbo(Session.GetHabbo());

            if (tour != null)
            {
                MithServer.GetGame().GetGuideManager().endSession(tour);
            }

        }
    }
}