﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Rooms.Notifications
{
    class NuxNotificationComposer : ServerPacket
    {
        public NuxNotificationComposer(int bubble = 0)
            : base(ServerPacketHeader.NuxNotificationMessageComposer)
        {
            switch (bubble)
            {
                case 0:
                    {
                        base.WriteString("helpBubble/add/BOTTOM_BAR_INVENTORY/Este es el inventario. Para colocar tus furnis, tan sólo tienes que arrastrarlos hasta el suelo.");
                        break;
                    }
                case 1:
                    {
                        base.WriteString("helpBubble/add/BOTTOM_BAR_NAVIGATOR/Este es el navegador. ¡Úsalo para explorar las miles de salas que hay en el hotel!");
                        break;
                    }
                case 2:
                    {
                        base.WriteString("helpBubble/add/chat_input/Puedes chatear con tus amigos escribiendo aquí.");
                        break;
                    }
                case 3:
                    {
                        base.WriteString("habbopages/welcome_message.txt");
                        break;
                    }
                case 4:
                    {
                        base.WriteString("habbopages/commands.txt");
                        break;
                    }

                case 5:
                    {
                        base.WriteString("habbopages/colores.txt");
                        break;
                    }
                case 6:
                    {
                        base.WriteString("habbopages/test.txt");
                        break;
                    }
                case 7:
                    {
                        base.WriteString("helpBubble/add/HELP_BUTTON/Utilizando este botón puedes pedir ayuda a un embajador o moderador");
                        break;
                    }
            }
        }
    }
}
