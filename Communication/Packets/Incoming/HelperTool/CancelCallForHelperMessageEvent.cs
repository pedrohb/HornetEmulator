﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.HelperTool;
using Plus.HabboHotel.Guides;

namespace Plus.Communication.Packets.Incoming.HelperTool
{
    class CancelCallForHelperMessageEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            GuideTour tour = MithServer.GetGame().GetGuideManager().getGuideTourByNoob(Session.GetHabbo());

            if (tour != null)
            {
                tour.end();
                MithServer.GetGame().GetGuideManager().endSession(tour);
            }


        }
    }
}
