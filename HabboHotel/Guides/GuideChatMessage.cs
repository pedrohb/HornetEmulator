﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Guides
{
    public class GuideChatMessage
    {
        public int userId;
        public string message;
        public int timestamp;

        public GuideChatMessage(int userId, string message, int timestamp)
        {
            this.userId = userId;
            this.message = message;
            this.timestamp = timestamp;
        }
    }

}
