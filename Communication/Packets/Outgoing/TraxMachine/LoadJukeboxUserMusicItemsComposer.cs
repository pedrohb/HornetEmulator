﻿using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Rooms;
using System;
using System.Collections.Generic;

namespace Plus.Communication.Packets.Outgoing.TraxMachine
{
    internal class LoadJukeboxUserMusicItemsComposer : ServerPacket
    {
        public LoadJukeboxUserMusicItemsComposer(Room room, GameClient Session = null) : base(ServerPacketHeader.LoadJukeboxUserMusicItemsMessageComposer)
        {
            List<Item> avaliableSongs = room.GetTraxManager().GetAvaliableSongs();
            base.WriteInteger(avaliableSongs.Count);
            foreach (Item current in avaliableSongs)
            {
                base.WriteInteger(current.Id);
                base.WriteInteger(Convert.ToInt32(current.ExtraData));
            }
        }

        public LoadJukeboxUserMusicItemsComposer(ICollection<Item> Items) : base(ServerPacketHeader.LoadJukeboxUserMusicItemsMessageComposer)
        {
            base.WriteInteger(Items.Count);
            foreach (Item current in Items)
            {
                base.WriteInteger(current.Id);
                base.WriteInteger(Convert.ToInt32(current.ExtraData));
            }
        }
    }
}
