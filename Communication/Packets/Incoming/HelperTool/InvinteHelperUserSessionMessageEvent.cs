﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.HelperTool;
using Plus.HabboHotel.Guides;
using Plus.Communication.Packets.Outgoing;

namespace Plus.Communication.Packets.Incoming.HelperTool
{
    class InvinteHelperUserSessionMessageEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {

            GuideTour tour = MithServer.GetGame().GetGuideManager().getGuideTourByHelper(Session.GetHabbo());

            if (tour != null)
            {
                ServerPacket message = new HelperSessionInvinteRoomMessageComposer(Session.GetHabbo().CurrentRoom);
                tour.getNoob().GetClient().SendMessage(message);
                tour.getHelper().GetClient().SendMessage(message);
            }
        }
    }
}