﻿using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Database.Interfaces;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace Plus.HabboHotel.Catalog.FurniMatic
{
    public class FurniMaticRewardsManager
    {
        private List<FurniMaticRewards> Rewards;
        public List<FurniMaticRewards> GetRewards() { return Rewards; }
        public List<FurniMaticRewards> GetRewardsByLevel(int level)
        {
            var rewards = new List<FurniMaticRewards>();
            foreach (var reward in Rewards.Where(furni => furni.Level == level))
                rewards.Add(reward);
            return rewards;
        }
        public FurniMaticRewards GetRandomReward(int Puntaje = 0, GameClient Session = null)
        {
            Random randnumbers = new Random();
            string porcentajeB = "0%", porcentajeA = "0%", porcentajeS = "0%", porcentajeSS = "0%", porcentajelost = "0%";
            string clase = "C";
            var level = 1; //rare level 2 100%
            if (Puntaje > 0 && Puntaje < 10) // (8 rares level 1) 0 (7 rares normales + 1 rare level 2)
            {
                if (randnumbers.Next(0, 100) == 99) //1%
                    level = 4; //rare level 5
                else if (randnumbers.Next(0, 50) == 49) //2%
                    level = 3; //rare level 4
                else if (randnumbers.Next(0, 5) == 4) //20%
                    level = 2; //rare level 3
                porcentajeB = "100%";
                porcentajeA = "20%";
                porcentajeS = "2%";
                porcentajeSS = "1%";
            }
            else if (Puntaje >= 10 && Puntaje < 12) //(6 rares level 1 + 2 rares level 2) o (5 rares level 1 + 3 rares level 2)
            {
                if (randnumbers.Next(0, 50) == 49) //2%
                    level = 4; //rare level 5
                else if (randnumbers.Next(0, 33) == 32)//3%
                    level = 3; //rare level 4
                else if (randnumbers.Next(0, 4) == 3)//25%
                    level = 2; //rare level 3
                porcentajeB = "100%";
                porcentajeA = "25%";
                porcentajeS = "3%";
                porcentajeSS = "2%";
            }
            else if (Puntaje >= 12 && Puntaje < 14)//(4 rares level 1 + 4 rares level 2) o (3 rares level 1 + 5 rares level 2)
            {
                if (randnumbers.Next(0, 40) == 39) //2.5%
                    level = 4; //rare level 5
                else if (randnumbers.Next(0, 20) == 19)//5%
                    level = 3; //rare level 4
                else if (randnumbers.Next(0, 3) == 2) //33,3%
                    level = 2; //rare level 3
                if (Session.GetHabbo().VIPRank > 0)
                {
                    porcentajelost = "0%";
                }
                else
                {
                    if (randnumbers.Next(0, 50) == 49) //2%
                        level = 0; //basura
                    porcentajelost = "2% (USUARIOS VIP: 0%)";
                }
                porcentajeB = "100%";
                porcentajeA = "33.3%";
                porcentajeS = "5%";
                porcentajeSS = "2.5%";
            }
            else if (Puntaje >= 14 && Puntaje < 16)//(6 rares level 2 + 2 rares level 1) ó (7 rares level 2 + 1 rare level 1)
            {
                if (randnumbers.Next(0, 35) == 34) //2.8%
                    level = 4; //rare level 5
                else if (randnumbers.Next(0, 14) == 13)//7%
                    level = 3; //rare level 4
                else if (randnumbers.Next(0, 2) == 1)//50%
                    level = 2; //rare level 3
                if (Session.GetHabbo().VIPRank > 0)
                {
                    porcentajelost = "0%";
                }
                else
                {
                    if (randnumbers.Next(0, 10) == 9) //10%
                        level = 0; //basura
                    porcentajelost = "10% (USUARIOS VIP: 0%)";
                }
                porcentajeB = "100%";
                porcentajeA = "50%";
                porcentajeS = "7%";
                porcentajeSS = "2.8%";
            }
            else if (Puntaje >= 16 && Puntaje < 18) // (8 rares level 2) o (7 rares level 2 + 1 rare level 3)
            {
                level = 2; //rare level 3 100%
                if (randnumbers.Next(0, 33) == 32)//3.03%
                    level = 4; //rare level 5
                else if (randnumbers.Next(0, 12) == 11)//8%
                    level = 3; //rare level 4
                if (Session.GetHabbo().VIPRank > 0)
                {
                    if (randnumbers.Next(0, 50) == 49) //2%
                        level = 0; //basura
                    porcentajelost = "2%";
                }
                else
                {
                    if (randnumbers.Next(0, 5) == 4) //20%
                        level = 0; //basura
                    porcentajelost = "20% (USUARIOS VIP: 2%)";
                }
                porcentajeB = "0%";
                porcentajeA = "100%";
                porcentajeS = "8%";
                porcentajeSS = "3%";

            }
            else if (Puntaje >= 18 && Puntaje < 20) //(6 rares level 2 + 2 rares level 3) o (5 rares level 2 + 3 rares level 3)
            {
                level = 2; //rare level 3 100%
                if (randnumbers.Next(0, 25) == 24)//4%
                    level = 4; //rare level 5
                else if (randnumbers.Next(0, 10) == 9)//10%
                    level = 3; //rare level 4
                if (Session.GetHabbo().VIPRank > 0)
                {
                    if (randnumbers.Next(0, 50) == 49) //5%
                        level = 0; //basura
                    porcentajelost = "2%";
                }
                else
                {
                    if (randnumbers.Next(0, 4) == 3) //35%
                        level = 0; //basura
                    porcentajelost = "25% (USUARIOS VIP: 2%)";
                }
                porcentajeB = "0%";
                porcentajeA = "100%";
                porcentajeS = "10%";
                porcentajeSS = "4%";

            }
            else if (Puntaje >= 20 && Puntaje < 22)// (4 rares level 2 + 4 rares level 3) ó (5 rares level 3 + 3 rares level 2)
            {
                level = 2; //rare level 3 100%
                if (randnumbers.Next(0, 20) == 19) //5%
                    level = 4; //rare level 5
                else if (randnumbers.Next(0, 8) == 7)//12%
                    level = 3; //rare level 4
                if (Session.GetHabbo().VIPRank > 0)
                {
                    if (randnumbers.Next(0, 20) == 19) //8%
                        level = 0; //basura
                    porcentajelost = "5%";
                }
                else
                {
                    if (randnumbers.Next(0, 4) == 3) //35%
                        level = 0; //basura
                    porcentajelost = "35% (USUARIOS VIP: 5%)";
                }
                porcentajeB = "0%";
                porcentajeA = "100%";
                porcentajeS = "12%";
                porcentajeSS = "5%";

            }
            else if (Puntaje >= 22 && Puntaje < 24)// (6 rares level 3 + 2 rares level 2) o (7 rares level 3 + 1 rare level 2)
            {
                level = 2; //rare level 3 100%
                if (randnumbers.Next(0, 10) == 9) //10%
                    level = 4; //rare level 5
                else if (randnumbers.Next(0, 6) == 5)//15%
                    level = 3; //rare level 4
                if (Session.GetHabbo().VIPRank > 0)
                {
                    if (randnumbers.Next(0, 10) == 9) //10%
                        level = 0; //basura
                    porcentajelost = "5%";
                }
                else
                {
                    if (randnumbers.Next(0, 4) == 3) //50%
                        level = 0; //basura
                    porcentajelost = "35% (USUARIOS VIP: 5%)";
                }
                porcentajeB = "0%";
                porcentajeA = "100%";
                porcentajeS = "15%";
                porcentajeSS = "10%";

            }
            else if (Puntaje >= 24) // 8 rares level 3
            {
                level = 3; //rare level 4 100%
                if (randnumbers.Next(0, 5) == 4) //20%
                    level = 4; //rare level 5
                if (Session.GetHabbo().VIPRank > 0)
                {
                    if (randnumbers.Next(0, 17) == 16) //15%
                        level = 0; //basura
                    porcentajelost = "15%";
                }
                else
                {
                    if (randnumbers.Next(0, 3) == 2) //50%
                        level = 0; //basura
                    porcentajelost = "40% (USUARIOS VIP: 15%)";
                }
                porcentajeB = "0%";
                porcentajeA = "0%";
                porcentajeS = "100%";
                porcentajeSS = "20%";
            }
            if (level == 1)
                clase = "B";
            else if (level == 2)
                clase = "A";
            else if (level == 3)
                clase = "S";
            else if (level == 4)
                clase = "SS";
            StringBuilder Lista = new StringBuilder();
            Lista.Append("El puntaje acumulado para realizar el reciclaje es: " + Puntaje + " \r\r");
            Lista.Append("Probabilidades en base a tu puntaje: \r");
            Lista.Append("Rare Clase B: " + porcentajeB + "\r");
            Lista.Append("Rare Clase A: " + porcentajeA + "\r");
            Lista.Append("Rare Clase S: " + porcentajeS + "\r");
            Lista.Append("Rare Clase SS: " + porcentajeSS + "\r");
            Lista.Append("PORCENTAJE DE FALLA: " + porcentajelost + "\r\r");
            if (level > 0)
                Lista.Append("Felicidades, en base a tu puntaje obtuviste un rare clase : " + clase);
            else
                Lista.Append("Aww... El crafteo falló");
            Session.SendMessage(new MOTDNotificationComposer(Lista.ToString()));
            if (level == 0)
            {
                return new FurniMaticRewards(0, 70004, 0); //mierda
            }
            var possibleRewards = GetRewardsByLevel(level);
            if (possibleRewards != null && possibleRewards.Count >= 1)
                return possibleRewards[randnumbers.Next(0, (possibleRewards.Count))];
            else
                return new FurniMaticRewards(0, 470, 0); // eco_lamp*2 asi c iama creo xd tienes que cambiar el 470 por la id
        }

        public void Initialize(IQueryAdapter dbClient)
        {
            Rewards = new List<FurniMaticRewards>();
            dbClient.SetQuery("SELECT display_id, item_id, reward_level FROM ecotron_rewards");
            var table = dbClient.getTable();
            if (table == null)
                return;
            foreach (DataRow row in table.Rows)
                Rewards.Add(new FurniMaticRewards(Convert.ToInt32(row["display_id"]), Convert.ToInt32(row["item_id"]), Convert.ToInt32(row["reward_level"])));
        }
    }
}