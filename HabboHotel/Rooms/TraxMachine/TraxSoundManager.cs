﻿using log4net;
using Plus.Database;
using Plus.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;

namespace Plus.HabboHotel.Rooms.TraxMachine
{
    public class TraxSoundManager
    {
        public static List<TraxMusicData> Songs = new List<TraxMusicData>();

        private static ILog Log = LogManager.GetLogger("Plus.HabboHotel.Rooms.TraxMachine");

        public void Init()
        {
            TraxSoundManager.Songs.Clear();
            DataTable table;
            using (IQueryAdapter queryReactor = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                queryReactor.RunQuery("SELECT * FROM jukebox_songs_data");
                table = queryReactor.getTable();
            }
            foreach (DataRow row in table.Rows)
            {
                TraxSoundManager.Songs.Add(TraxMusicData.Parse(row));
            }
            TraxSoundManager.Log.Info("Habbo Trax Machine Manager -> Sounds Loaded Sucessfully [ " + TraxSoundManager.Songs.Count + " Songs ]");
        }

        public static TraxMusicData GetMusic(int id)
        {
            TraxMusicData result;
            foreach (TraxMusicData current in TraxSoundManager.Songs)
            {
                bool flag = current.Id == id;
                if (flag)
                {
                    result = current;
                    return result;
                }
            }
            result = null;
            return result;
        }
    }
}
