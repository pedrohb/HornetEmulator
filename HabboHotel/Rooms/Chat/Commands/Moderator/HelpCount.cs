﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Plus.HabboHotel.Users;
using Plus.HabboHotel.GameClients;

using Plus.Database.Interfaces;
using Plus.Communication.Packets.Outgoing.Notifications;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class HelpCount : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_lincecount"; }
        }

        public string Parameters
        {
            get { return "()"; }
        }

        public string Description
        {
            get { return "Muestra todos los logs de ayuda de los usuarios con rango 2 (linces)"; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            DataTable Tabla = null;
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT count(lince_helpalert.id), users.username FROM lince_helpalert INNER JOIN users ON lince_helpalert.lince_id = users.id WHERE users.rank = 2 GROUP BY users.username ORDER BY users.username");
                Tabla = dbClient.getTable();
            }
            StringBuilder Lista = new StringBuilder();
            Lista.Append("Número de reportes de ayuda registrados por Lince: \r\r");
            foreach (DataRow Row in Tabla.Rows)
            {
                Lista.Append("Usuario: " + Convert.ToString(Row[1]) + " - Reportes registrados: " + Convert.ToString(Row[0]) + "\r\r");
            }
            Session.SendMessage(new MOTDNotificationComposer(Lista.ToString()));
            return;
        }
    }
}