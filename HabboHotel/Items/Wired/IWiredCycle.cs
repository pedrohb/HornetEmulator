﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Items.Wired
{
    interface IWiredCycle
    {
        double Delay { get; set; }
        double TickCount { get; set; }
        bool OnCycle();
    }
}
