﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.HelperTool;
using Plus.HabboHotel.Guides;
using Plus.Database.Interfaces;

namespace Plus.Communication.Packets.Incoming.HelperTool
{
    class CallForHelperMessageEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            int type = Packet.PopInt();
            string message = Packet.PopString();


            GuideTour activeTour = MithServer.GetGame().GetGuideManager().getGuideTourByHabbo(Session.GetHabbo());

            if (activeTour != null)
            {
                Session.SendMessage(new ErrorHelpRequestComposer(0, null));
                return;
            }

            GuideTour Tour = new GuideTour(Session.GetHabbo(), message);
            Tour.setStartTime(Convert.ToInt32(MithServer.GetUnixTimestamp()));
            Tour.activated = true;
            if (MithServer.GetGame().GetGuideManager().findHelper(Tour))
            {
                using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                {
                    int Id = Session.GetHabbo().Id;
                    dbClient.RunQuery("INSERT INTO `lince_helpalert`(`id_user`, `mensaje`) VALUES ('" + Id + "','" + message + "')");

                }
            }


        }
    }
}
