﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Utilities;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Groups;
using Plus.HabboHotel.Users;


namespace Plus.Communication.Packets.Outgoing.Rooms.Engine
{
     class ObjectUpdateComposer : ServerPacket
    {
         public ObjectUpdateComposer(Item Item, int UserId)
             : base(ServerPacketHeader.ObjectUpdateMessageComposer)
         {
            base.WriteInteger(Item.Id);
            base.WriteInteger(Item.GetBaseItem().SpriteId);
            base.WriteInteger(Item.GetX);
            base.WriteInteger(Item.GetY);
            base.WriteInteger(Item.Rotation);
            base.WriteString(String.Format("{0:0.00}", TextHandling.GetString(Item.GetZ)));
            base.WriteString(String.Empty);

            if (Item.LimitedNo > 0)
            {
                base.WriteInteger(1);
                base.WriteInteger(256);
               base.WriteString(Item.ExtraData);
                base.WriteInteger(Item.LimitedNo);
                base.WriteInteger(Item.LimitedTot);
            }
            else if (Item.Data.InteractionType == InteractionType.PINATA/* || Item.Data.InteractionType == InteractionType.PINATATRIGGERED*/)
            {
                base.WriteInteger(0);
                base.WriteInteger(7);
                if (Item.ExtraData.Length <= 0)
                {
                    base.WriteString("6");
                    base.WriteInteger(0);
                }
                else
                {
                    base.WriteString((int.Parse(Item.ExtraData) == 1) ? "8" : "6");
                    base.WriteInteger(int.Parse(Item.ExtraData));
                }
                base.WriteInteger(1);
            }
            else if (Item.Data.InteractionType == InteractionType.PINATA/* || Item.Data.InteractionType == InteractionType.PINATATRIGGERED*/)
            {
                base.WriteInteger(0);
                base.WriteInteger(7);
                if (Item.ExtraData.Length <= 0)
                {
                    base.WriteString("6");
                    base.WriteInteger(0);
                }
                else
                {
                    base.WriteString((int.Parse(Item.ExtraData) == 1) ? "8" : "6");
                    base.WriteInteger(int.Parse(Item.ExtraData));
                }
                base.WriteInteger(1);
            }
            else
            {
                ItemBehaviourUtility.GenerateExtradata(Item, this);
            }
          
            base.WriteInteger(-1); // to-do: check
            base.WriteInteger((Item.GetBaseItem().Modes > 1) ? 1 : 0);
            base.WriteInteger(UserId);
        }
    }
}