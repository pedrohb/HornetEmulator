﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Items;
using Plus.Communication.Packets.Outgoing.Notifications;

using Plus.Database.Interfaces;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;

namespace Plus.Communication.Packets.Incoming.Rooms.Nux
{
    class ClosedNuxNotification : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            int Count = Session.GetHabbo().NuxClosedCount;
            if (Count == 1)
            {
                Session.SendMessage(new NuxNotificationComposer(1));
                Session.GetHabbo().NuxClosedCount++;
            }
            else if (Count == 2)
            {
                Session.SendMessage(new NuxNotificationComposer(2));
                Session.GetHabbo().NuxClosedCount++;
            }
            else if (Count == 3)
            {
                Session.GetHabbo().NuxClosedCount = 0;
            }
            else
            {
                Session.SendMessage(new NuxNotificationComposer(0));
                Session.GetHabbo().NuxClosedCount++;
            }
        }
    }
}
