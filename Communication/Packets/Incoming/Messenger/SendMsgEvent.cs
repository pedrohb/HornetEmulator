﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.Messenger;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.Groups;

namespace Plus.Communication.Packets.Incoming.Messenger
{
    class SendMsgEvent : IPacketEvent
    {

        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().GetMessenger() == null)
                return;

            int userId = Packet.PopInt();
            if (userId == 0 || userId == Session.GetHabbo().Id)
                return;

            string message = MithServer.GetGame().GetChatManager().GetFilter().CheckMessage(Packet.PopString());
            if (string.IsNullOrWhiteSpace(message))
                return;

            if (MithServer.GetGame().GetChatManager().GetFilter().IsFiltered(message))
            {
                MithServer.GetGame().GetClientManager().StaffAlert(new RoomNotificationComposer("Alerta de Publicidad",
                              "El Usuario: <b>" + Session.GetHabbo().Username + "<br>" +

                              "<br></b> Esta Publicando y/o usando una palabra contenida en el filtro " + "<br>" +
                              "<br></b> Mediante : <b> Chat Por Consola <br>" +
                              "<br><b>La Palabra Usada Fue:</b><br>" +
                                "<br>" + "<b>" + "<font color =\"#06087F\">" + message + "</font>" + "</b><br>" +
                              "<br>Para ir a la sala, da clic en \"Ir a la Sala \"</b>",
                              "filter", "Ir a la Sala", "event:navigator/goto/" + Session.GetHabbo().CurrentRoomId));
                message = "Estoy Intentando Publicar Otro Hotel Porfavor Advierte a un Staff";
            }


            if (Session.GetHabbo().TimeMuted > 0)
            {
                Session.SendNotification("Estas muteado, no puedes enviar mensajes..");
                return;
            }
            if (userId == 0)
            {

                MithServer.GetGame().GetClientManager().StaffAlert(new NewConsoleMessageComposer(0x7fffffff, Session.GetHabbo().Username + ": " + message), Session.GetHabbo().Id);
                //MithServer.GetGame().GetClientManager().StaffAlert(new NewConsoleMessageComposer(Session.GetHabbo().Id, message, 0, userId), Session.GetHabbo().Id);
                return;
            }
            if (userId < 0)
            {
                MithServer.GetGame().GetClientManager().GroupMessage(new NewConsoleMessageComposer(Session.GetHabbo().Id, message, 0, userId), Session.GetHabbo().Id, -userId);
                return;
            }
            
            Session.GetHabbo().GetMessenger().SendInstantMessage(userId, message);

        }
    }
}