﻿using System;
using System.Linq;
using System.Text;
using System.Collections;
using System.Collections.Generic;
using System.Collections.Concurrent;

using Plus.Communication.Packets.Incoming;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;
using System.Globalization;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Items.Wired.Boxes.Effects
{
    class AddGiveScoreBox : IWiredItem, IWiredCycle
    {
        public Room Instance { get; set; }
        public Item Item { get; set; }
        public WiredBoxType Type { get { return WiredBoxType.EffectGiveScore; } }
        public ConcurrentDictionary<int, Item> SetItems { get; set; }
        public string StringData { get; set; }
        public bool BoolData { get; set; }
        public double Delay { get { return this._delay; } set { this._delay = value; this.TickCount = value; } }
        public double TickCount { get; set; }
        public string ItemsData { get; set; }

        private Queue _queue;
        private double _delay = 0;

        public AddGiveScoreBox(Room Instance, Item Item)
        {
            this.Instance = Instance;
            this.Item = Item;
            this.SetItems = new ConcurrentDictionary<int, Item>();

            this._queue = new Queue();
            this.TickCount = Delay;
        }

        public void HandleSave(ClientPacket Packet)
        {
            int Unknown = Packet.PopInt();
            int score = Packet.PopInt();
            int times = Packet.PopInt();
            string Unknown2 = Packet.PopString();
            int Unknown3 = Packet.PopInt();
            double Delay = Packet.PopInt();

            this.Delay = Delay;
            this.StringData = Convert.ToString(score + ";" + times);

            // this.Delay = Packet.PopInt();
        }

        public bool OnCycle()
        {
            if (_queue.Count == 0)
            {
                this._queue.Clear();
                this.TickCount = Delay;
                return true;
            }

            while (_queue.Count > 0)
            {
                Habbo Player = (Habbo)_queue.Dequeue();
                if (Player == null || Player.CurrentRoom != Instance)
                    continue;

                this.TeleportUser(Player);
            }

            this.TickCount = Delay;
            return true;
        }

        public bool Execute(params object[] Params)
        {
            if (Params == null || Params.Length == 0)
                return false;

            Habbo Player = (Habbo)Params[0];

            if (Player == null)
                return false;

            this._queue.Enqueue(Player);
            return true;
        }

        private void TeleportUser(Habbo Player)
        {
            RoomUser User = Player.CurrentRoom.GetRoomUserManager().GetRoomUserByHabbo(Player.Id);
            if (User == null)
                return;

            Room Instance = Player.CurrentRoom;

            int mScore = int.Parse(StringData.Split(';')[0]) * int.Parse(StringData.Split(';')[1]);

            if (Instance != null || User != null)
            {
                //bool HighScoreHere = false;

                if (Instance.GetRoomItemHandler().wf_highscore_count != 0)
                {
                    foreach (Item item in Instance.GetRoomItemHandler().GetFloor.ToList())
                    {
                        if (item.GetBaseItem().InteractionType == InteractionType.WIRED_HIGHSCORE)
                        {

                            string Type = item.GetBaseItem().ItemName.Split('_')[1].Split('*')[0];
                            int Sort = Convert.ToInt32(item.GetBaseItem().ItemName.Split('_')[1].Split('*')[1]);
                            Instance.GetRoomItemHandler().InsertScoreOnWired(User.GetUsername() + ",", mScore, Type, Sort, item);
                        }
                    }
                }
                else
                    return;
                
                //if (HighScoreHere)
                //{
                //    Instance.GetRoomItemHandler().UpdateWiredScoreBoard(); //<<<<<
                //    //User.GetClient().SendWhisper(string.Concat("Has ganado ", mScore, " puntos en la clasificación."));
                //}
            }
            else
                return;
        }
    }
}