﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Items;

namespace Plus.Communication.Packets.Outgoing.Rooms.Engine
{
    class ItemAddComposer : ServerPacket
    {
        public ItemAddComposer(Item Item)
            : base(ServerPacketHeader.ItemAddMessageComposer)
        {
            string name = "";
            base.WriteString(Item.Id.ToString());
            base.WriteInteger(Item.GetBaseItem().SpriteId);
            base.WriteString(Item.wallCoord != null ? Item.wallCoord : string.Empty);

            ItemBehaviourUtility.GenerateWallExtradata(Item, this);

            base.WriteInteger(-1);
            base.WriteInteger((Item.GetBaseItem().Modes > 1) ? 1 : 0); // Type New R63 ('use bottom')
            base.WriteInteger(Item.UserID);
            name = Item.Username + " " + clase(Item.GetBaseItem().rare_level);
            base.WriteString(name);
        }
        string clase(int level)
        {
            string clase = "(Furni Común)";
            if (level == 1)
                clase = "(Rare Clase: C)";
            else if (level == 2)
                clase = "(Rare Clase: B)";
            else if (level == 3)
                clase = "(Rare Clase: A)";
            else if (level == 4)
                clase = "(Rare Clase: S)";
            else if (level == 5)
                clase = "(Rare Clase: SS)";
            return clase;
        }
    }
}