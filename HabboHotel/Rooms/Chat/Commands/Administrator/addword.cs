﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Plus.HabboHotel.Users;
using Plus.HabboHotel.GameClients;

using Plus.Database.Interfaces;


namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class AddWord : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_add_word"; }
        }

        public string Parameters
        {
            get { return "%Filtro%"; }
        }

        public string Description
        {
            get { return "Inserta una nueva palabra al filtro del hotel"; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Por favor escribe una palabra válida");
                return;
            }

            DataRow Filter = null;
            string Palabra = Params[1];
            string User = null;

            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `word` FROM wordfilter WHERE `word` = @Palabra LIMIT 1");
                dbClient.AddParameter("Palabra", Palabra);
                Filter = dbClient.getRow();
            }

            if (Filter != null)
            {
                Session.SendNotification("Ya existe la palabra (" + Palabra + ") en el filtro del hotel.");
                return;
            }
            else
            {
                User = Session.GetHabbo().Username;

                using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                {
                    dbClient.RunQuery("INSERT INTO wordfilter (`word`,`addedby`,`bannable`) VALUES ('" + Convert.ToString(Palabra) + "','" + User + "', '1')");
                }
                MithServer.GetGame().GetChatManager().GetFilter().Init();
                Session.SendWhisper("La palabra " + Palabra + " ha sido añadida al filtro del hotel");
            }
        }
    }
}
