﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Plus.HabboHotel.Users;
using Plus.HabboHotel.GameClients;

using Plus.Database.Interfaces;
using Plus.Communication.Packets.Outgoing.Notifications;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class UsersOnCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_userson"; }
        }

        public string Parameters
        {
            get { return "(username)"; }
        }

        public string Description
        {
            get { return "Muestra toda la información de un usuario."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            Dictionary<Habbo, UInt32> clients = new Dictionary<Habbo, UInt32>();

            foreach (var client in  MithServer.GetGame().GetClientManager().GetClients.ToList())
            {
                if (client != null && client.GetHabbo() != null && client.GetHabbo().Rank > 0)
                    clients.Add(client.GetHabbo(), Convert.ToUInt16(client.GetHabbo().Rank));
            }
            StringBuilder Lista = new StringBuilder();
            DataTable Tabla = null;
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT count(users.id) FROM users WHERE users.online = '1'");
                Tabla = dbClient.getTable();
            }
            foreach (DataRow Row in Tabla.Rows)
                Lista.Append("Hay " + Convert.ToString(Row[0]) + " usuarios conectados:  \r\r");
            foreach (KeyValuePair<Habbo, UInt32> client in clients.OrderBy(key => key.Value))
            {
                Lista.Append("" + client.Key.GetClient().GetHabbo().Username + "\r");
            }
            Session.SendMessage(new MOTDNotificationComposer(Lista.ToString()));
            return;
        }
    }
}
