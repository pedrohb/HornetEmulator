﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.HabboHotel.Users;
using Plus.HabboHotel.Users.Messenger;
using Plus.HabboHotel.Users.Relationships;
using Plus.HabboHotel.Groups;

namespace Plus.Communication.Packets.Outgoing.Messenger
{
    class BuddyListComposer : ServerPacket
    {
        public BuddyListComposer(ICollection<MessengerBuddy> friends, Habbo player, int pages, int page)
            : base(ServerPacketHeader.BuddyListMessageComposer)
        {
            List<Group> Groups = MithServer.GetGame().GetGroupManager().GetGroupForUserByFavouriteGroupId(player.GetStats().FavouriteGroupId, player.Id);
            base.WriteInteger(pages);// Pages
            base.WriteInteger(page);// Page
            base.WriteInteger(friends.Count + Groups.Count);
            if (player.Rank >= 2)
            {
                base.WriteInteger(0x7fffffff);
                base.WriteString("[" + MithServer.GetConfig().data["hotel.name"].ToString() + "] " +  "Staff");
                base.WriteInteger(1);//Gender.
                base.WriteBoolean(true);
                base.WriteBoolean(false);
                base.WriteString("hr-831-45.fa-1206-91.sh-290-1331.ha-3129-100.hd-180-2.cc-3039-73.ch-3215-92.lg-270-73");
                base.WriteInteger(0); // category id
                base.WriteString(string.Empty);
                base.WriteString(string.Empty);//Alternative name?
                base.WriteString(string.Empty);
                base.WriteBoolean(true);
                base.WriteBoolean(false);
                base.WriteBoolean(false);//Pocket Habbo user.
                base.WriteShort(0);
            }
            if (player.GetStats().FavouriteGroupId > 0)
            {
                foreach (Group Group in Groups)
                {
                    base.WriteInteger(-Group.Id);
                    base.WriteString(Group.Name);
                    base.WriteInteger(1);//Gender.
                    base.WriteBoolean(true);
                    base.WriteBoolean(false);
                    base.WriteString(Group.Badge);
                    base.WriteInteger(0); // category id
                    base.WriteString(string.Empty);
                    base.WriteString(string.Empty);//Alternative name?
                    base.WriteString(string.Empty);
                    base.WriteBoolean(true);
                    base.WriteBoolean(false);
                    base.WriteBoolean(false);//Pocket Habbo user.
                    base.WriteShort(0);
                }
            }

            foreach (MessengerBuddy Friend in friends.ToList())
            {
                Relationship Relationship = player.Relationships.FirstOrDefault(x => x.Value.UserId == Convert.ToInt32(Friend.UserId)).Value;

                base.WriteInteger(Friend.Id);
                base.WriteString(Friend.mUsername);
                base.WriteInteger(1);//Gender.
                base.WriteBoolean(Friend.IsOnline);
                base.WriteBoolean(Friend.IsOnline && Friend.InRoom);
                base.WriteString(Friend.IsOnline ? Friend.mLook : string.Empty);
                base.WriteInteger(0); // category id
                base.WriteString(Friend.IsOnline ? Friend.mMotto : string.Empty);
                base.WriteString((Friend.mUsername == "O" ? "Fresco" : string.Empty));//Alternative name?
                base.WriteString(string.Empty);
                base.WriteBoolean(true);
                base.WriteBoolean(false);
                base.WriteBoolean(false);//Pocket Habbo user.
                base.WriteShort(Relationship == null ? 0 : Relationship.Type);
            }

        }
    }
}
