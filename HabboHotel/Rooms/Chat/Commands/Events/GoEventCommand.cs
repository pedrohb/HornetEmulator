﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Session;
using Plus.Database.Interfaces;
using System.Data;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Events
{
    class GoEventCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_goevent"; }
        }

        public string Parameters
        {
            get { return "%username%"; }
        }

        public string Description
        {
            get { return "Si hay un evento disponible; ¡A jugar!"; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            DataRow Row = null;
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT room_id FROM events_logs WHERE active = 1 ORDER BY id desc LIMIT 1");
                Row = dbClient.getRow();
            }
            if (Row == null)
            {
                Session.SendWhisper("No hay eventos activos en este momento.");
                return;
            }
            if(Session.GetHabbo().CurrentRoomId == Convert.ToInt32(Row[0]))
            {
                Session.SendWhisper("Ya te encuentras en la sala del evento!");
                return;
            }
            Session.GetHabbo().PrepareRoom(Convert.ToInt32(Row[0]), "");
        }
    }
}