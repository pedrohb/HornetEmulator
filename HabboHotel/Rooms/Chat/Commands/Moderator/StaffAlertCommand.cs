﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Communication.Packets.Outgoing.Moderation;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class StaffAlertCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_staff_alert"; }
        }

        public string Parameters
        {
            get { return "%message%"; }
        }

        public string Description
        {
            get { return "Sends a message typed by you to the current online staff members."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Inserta el mensaje que desea enviar");
                return;
            }

            string Message = CommandManager.MergeParams(Params, 1);
            MithServer.GetGame().GetClientManager().StaffAlert(new WhisperComposer(0, "Staff Chat - " + Session.GetHabbo().Username + ": " + Message, 0, 34));
            return;
        }
    }
}