﻿using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.HelperTool;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Users;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Guides
{
    public class GuideManager
    {
        private List<GuideTour> activeTours;
        //private THashSet<GuardianTicket> activeTickets;
        //private THashSet<GuardianTicket> closedTickets;  
        private ConcurrentDictionary<Habbo, bool> activeHelpers;
        //private THashMap<Habbo, GuardianTicket> activeGuardians;
        private ConcurrentDictionary<int, int> tourRequestTiming;

        public GuideManager()
        {
            this.activeTours = new List<GuideTour>();
            //this.activeTickets = new THashSet<GuardianTicket>();
            //this.closedTickets = new THashSet<GuardianTicket>();
            this.activeHelpers = new ConcurrentDictionary<Habbo, bool>();
            //this.activeGuardians = new THashMap<Habbo, GuardianTicket>();
            this.tourRequestTiming = new ConcurrentDictionary<int, int>();
        }

        public void userLogsOut(Habbo habbo)
        {
            GuideTour tour = null;
            tour = this.getGuideTourByHabbo(habbo);

            if (tour != null)
            {
                this.endSession(tour);
            }
            bool value;
            this.activeHelpers.TryRemove(habbo, out value);

            //GuardianTicket ticket = this.getTicketForGuardian(habbo);

            //if (ticket != null)
            //{
            //  ticket.removeGuardian(habbo);
            //}

            //this.activeGuardians.remove(habbo);
        }
        public void setOnGuide(Habbo habbo, bool onDuty)
        {
            if (onDuty)
            {
                this.activeHelpers.TryAdd(habbo, false);
            }
            else
            {
                GuideTour tour = this.getGuideTourByHabbo(habbo);

                if (tour != null)
                    return;
                bool value;
                this.activeHelpers.TryRemove(habbo, out value);
            }
        }
        public async void RequestFindNewHelper(GuideTour tour, KeyValuePair<Habbo, bool> Helper)//GuideFindNewHelper GuideFNH)
        {
            await PutTaskDelay();
            if (tour.activated)
            {
                if (!tour.helping)//!Helper.Key.helping)
                {
                    Helper.Key.HelperPendingRequest = false;
                    Helper.Key.GetClient().SendMessage(new EndHelperSessionMessageComposer(1)); //Case_closed
                    Helper.Key.GetClient().SendMessage(new CloseHelperSessionMessageComposer()); // composer sin codigo ?
                    if (!tour.beinghelped)
                    {
                        tour.addDeclinedHelper(Helper.Key.Id);
                        findHelper(tour);
                    }
                }
                else
                {
                    tour.helping = true;
                }
            }

        }
        async Task PutTaskDelay()
        {
            await Task.Delay(30000);
        }
        public bool findHelper(GuideTour tour)
        {
            foreach (KeyValuePair<Habbo, bool> Helper in this.activeHelpers)
            {
                if (!Helper.Value && Helper.Key.GetPermissions().HasRight("helper_tool"))
                {
                    if (!tour.hasDeclined(Helper.Key.Id) && !Helper.Key.HelperPendingRequest && !tour.helping)//&& !Helper.Key.GetPermissions().HasRight("helper_tool"))//!Helper.Key.HelperHelping) 
                    {
                        tour.checkSum++;
                        tour.setHelper(Helper.Key);
                        Helper.Key.GetClient().SendMessage(new CallForHelperWindowMessageComposer(tour, true));
                        tour.getNoob().GetClient().SendMessage(new CallForHelperWindowMessageComposer(tour, false));
                        this.activeTours.Add(tour);
                        RequestFindNewHelper(tour, Helper);
                        return true;
                    }
                    else
                    {
                        this.activeTours.Remove(tour);
                        continue;
                    }
                }
            }
            tour.activated = false;
            tour.getNoob().GetClient().SendMessage(new ErrorHelpRequestComposer(1, tour.getNoob().GetClient()));  //Error: No helper aviable

            return false;
        }

        public void declineTour(GuideTour tour)
        {
            Habbo helper = tour.getHelper();
            //helper.HelperHelping = false;
            helper.HelperPendingRequest = false;
            tour.addDeclinedHelper(helper.Id);
            tour.helping = false;
            helper.GetClient().SendMessage(new EndHelperSessionMessageComposer(1)); //Case_closed
            helper.GetClient().SendMessage(new CloseHelperSessionMessageComposer()); // composer sin codigo ?
                                                                                     //if (!tour.beinghelped)
                                                                                     //{
            if (!this.findHelper(tour)) //aca
            {
                this.endSession(tour);
                tour.getNoob().GetClient().SendMessage(new ErrorHelpRequestComposer(1, tour.getNoob().GetClient())); //No helpers aviable 
            }
            //}
        }

        public void startSession(GuideTour tour, Habbo helper)
        {
            this.activeHelpers.TryAdd(helper, true);
            helper.HelperPendingRequest = false;
            tour.helping = true;
            //helper.HelperHelping = true;
            tour.beinghelped = true;
            ServerPacket message = new InitHelperSessionChatMessageComposer(tour);
            tour.getNoob().GetClient().SendMessage(message);
            tour.getHelper().GetClient().SendMessage(message);
            tour.checkSum++;
            this.tourRequestTiming.TryAdd(tour.getStartTime(), Convert.ToInt32(MithServer.GetUnixTimestamp()));

        }

        public void endSession(GuideTour tour)
        {
            tour.activated = false;
            tour.beinghelped = false;
            tour.getNoob().GetClient().SendMessage(new EndHelperSessionMessageComposer(1)); //case closed
            tour.end();

            if (tour.getHelper() != null)
            {
                tour.helping = false;
                //tour.getHelper().HelperHelping = false;
                this.activeHelpers.TryAdd(tour.getHelper(), false);
                tour.getHelper().GetClient().SendMessage(new EndHelperSessionMessageComposer(1));
                tour.getHelper().GetClient().SendMessage(new CloseHelperSessionMessageComposer()); //composer vacio ?
                tour.getHelper().GetClient().SendMessage(new HandleHelperToolMessageComposer(true));
            }
        }
        public void recommend(GuideTour tour, bool recommend)
        {

            tour.setWouldRecommend(recommend ? GuideRecommendStatus.YES : GuideRecommendStatus.NO);
            tour.getNoob().GetClient().SendMessage(new CloseHelperSessionMessageComposer()); //composer vacio ?


            this.activeTours.Remove(tour);
        }

        public GuideTour getGuideTourByHelper(Habbo helper)
        {
            foreach (GuideTour tour in this.activeTours)
            {
                if (!tour.isEnded() && tour.getHelper() == helper)
                {
                    return tour;
                }
            }

            return null;
        }
        public GuideTour getGuideTourByNoob(Habbo noob)
        {
            foreach (GuideTour tour in this.activeTours)
            {
                if (tour.getNoob() == noob)
                {
                    return tour;
                }
            }

            return null;
        }

        public GuideTour getGuideTourByHabbo(Habbo habbo)
        {
            foreach (GuideTour tour in this.activeTours)
            {
                if (tour.getHelper() == habbo || tour.getNoob() == habbo)
                {
                    return tour;
                }
            }

            return null;
        }

        public int getGuidesCount()
        {
            return this.activeHelpers.Count;
        }
    }
}
