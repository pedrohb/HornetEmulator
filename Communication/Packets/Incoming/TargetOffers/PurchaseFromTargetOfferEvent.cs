﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plus.HabboHotel.LandingView;
using Plus.HabboHotel.LandingView.Promotions;
using Plus.HabboHotel.Catalog;
using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Items.Utilities;
using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Communication.Packets.Outgoing.Catalog;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using System.Data;
namespace Plus.Communication.Packets.Incoming.TargetOffers
{
    class PurchaseFromTargetOfferEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            int offerId = Packet.PopInt();
            int Amount = Packet.PopInt();
            int cantidad = Packet.PopInt();
            string FurniReward = Convert.ToString(MithServer.GetGame().GetTargetOffersManager().ItemId());
            if (!MithServer.GetGame().GetTargetOffersManager().OfferIsEnable())
            {
                Session.SendNotification("Lo sentimos, la ofera ha sido desactivada por un adminsitrador.");
                return;
            }
            if (!string.IsNullOrWhiteSpace(FurniReward))
            {
                DataRow FurniTable = null;
                using (var dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                {
                    dbClient.SetQuery("SELECT public_name FROM furniture WHERE id = '" + Convert.ToInt32(FurniReward) + "'");
                    FurniTable = dbClient.getRow();
                }
                Session.SendMessage(new PurchaseOKComposer());

                int TotalCreditsCost = MithServer.GetGame().GetTargetOffersManager().OfferCreditsPrice() * Amount;
                int TotalDiamondCost = MithServer.GetGame().GetTargetOffersManager().OfferDiamondsPrice() * Amount;
                if (Session.GetHabbo().Credits < TotalCreditsCost || Session.GetHabbo().Diamonds < TotalDiamondCost)
                {
                    Session.SendNotification("No posees suficientes créditos o diamantes para comprar esta oferta.");
                    return;
                }
                if (TotalCreditsCost > 0)
                {
                    Session.GetHabbo().Credits -= TotalCreditsCost;
                    Session.SendMessage(new CreditBalanceComposer(Session.GetHabbo().Credits));
                }
                if (TotalDiamondCost > 0)
                {
                    Session.GetHabbo().Diamonds -= TotalDiamondCost;
                    Session.SendMessage(new HabboActivityPointNotificationComposer(Session.GetHabbo().Diamonds, 0, 5));
                }
                //Session.SendNotification(MithServer.GetGame().GetTargetOffersManager().OfferID().ToString());
                if (Amount < 2)
                {
                    Session.GetHabbo().GetInventoryComponent().AddNewItem(0, Convert.ToInt32(FurniReward), Convert.ToString(FurniTable["public_name"]), 0, true, false, 0, 0);
                    Session.GetHabbo().GetInventoryComponent().UpdateItems(false);
                    MithServer.GetGame().GetTargetOffersManager().AddLogUserOffer(Session.GetHabbo().Id, MithServer.GetGame().GetTargetOffersManager().OfferID());
                }
                else
                {
                    for (int i = 0; i < Amount; i++)
                    {
                        Session.GetHabbo().GetInventoryComponent().AddNewItem(0, Convert.ToInt32(FurniReward), Convert.ToString(FurniTable["public_name"]), cantidad, true, false, 0, 0);
                        Session.GetHabbo().GetInventoryComponent().UpdateItems(false);
                        MithServer.GetGame().GetTargetOffersManager().AddLogUserOffer(Session.GetHabbo().Id, MithServer.GetGame().GetTargetOffersManager().OfferID());
                    }
                }
            }

        }
    }
}