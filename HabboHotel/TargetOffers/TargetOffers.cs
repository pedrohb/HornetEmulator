﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using Plus.Database.Interfaces;
using log4net;
using System.Threading;
using System.Data;

namespace Plus.HabboHotel.TargetOffers
{
    public class TargetOffers
    {
        public string _itemName { get; set; }
        public string _offerTitle { get; set; }
        public string _offerDescription { get; set; }
        public int _offerCreditsPrice { get; set; }
        public int _offerDiamondsPrice { get; set; }
        private int _offerTime { get; set; }
        private int _offerMaxBuy { get; set; }
        private string _offerBadgeReward { get; set; }
        private string _offerIconImage { get; set; }
        private string _offerImage { get; set; }
        private bool _offerIsEnabled { get; set; }

        public TargetOffers(string ItemName, string OfferTitle, string OfferDescription, int OfferCreditsPrice, int OfferDiamondsPrice, int OfferTime, int OfferMaxBuy, string OfferBadgeReward, string OfferIconImage, string OfferImage, bool OfferIsEnabled){
            this._itemName = ItemName;
            this._offerTitle = OfferTitle;
            this._offerDescription = OfferDescription;
            this._offerCreditsPrice = OfferCreditsPrice;
            this._offerDiamondsPrice = OfferDiamondsPrice;
            this._offerTime = OfferTime;
            this._offerMaxBuy = OfferMaxBuy;
            this._offerBadgeReward = OfferBadgeReward;
            this._offerIconImage = OfferIconImage;
            this._offerImage = OfferImage;
            this._offerIsEnabled = OfferIsEnabled;
        }
        
        public string ItemName()
        {
            return this._itemName;
        }

        public string OfferTitle()
        {
            return this._offerTitle;
        }

        public string OfferDescription()
        {
            return this._offerDescription;
        }

        public int OfferCreditsPrice()
        {
            return this._offerCreditsPrice;
        }

        public int OfferDiamondsPrice()
        {
            return this._offerDiamondsPrice;
        }

        public int OfferTime()
        {
            return this._offerTime;
        }

        public int OfferMaxBuy()
        {
            return this._offerMaxBuy;
        }

        public string OfferBadgeReward()
        {
            return this._offerBadgeReward;
        }

        public string OfferIconImage()
        {
            return this._offerIconImage;
        }

        public string OfferImage()
        {
            return this._offerImage;
        }

        public bool OfferIsEnabled()
        {
            return this._offerIsEnabled;
        }
    }
}