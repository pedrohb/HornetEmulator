﻿using System;
using System.Collections.Generic;
using System.Data;

using Plus.HabboHotel.Groups;
using Plus.Database.Interfaces;
using Plus.Communication.Packets.Outgoing;
using Plus.HabboHotel.Items;

namespace Plus.HabboHotel.Rooms
{
    public class RoomData
    {
        public int Id;
        public int AllowPets;
        public int AllowPetsEating;
        public int RoomBlockingEnabled;
        public int Category;
        public string Description;
        public string Floor;
        public int FloorThickness;
        public Group Group;
        public int Hidewall;
        public string Landscape;
        public string ModelName;
        public string Name;
        public string OwnerName;
        public int OwnerId;
        public string Password;
        public int Score;
        public RoomAccess Access;
        public List<string> Tags;
        public string Type;
        public int UsersMax;
        public int UsersNow;
        public int WallThickness;
        public string Wallpaper;
        public int WhoCanBan;
        public int WhoCanKick;
        public int WhoCanMute;
        private RoomModel mModel;
        public int chatMode;
        public int chatSpeed;
        public int chatSize;
        public int extraFlood;
        public int chatDistance;

        public Dictionary<int, List<KeyValuePair<int, string>>> WiredScoreBoardDay;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredScoreBoardWeek;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredScoreBoardMonth;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredScoreBoardAllTime;

        public Dictionary<int, List<KeyValuePair<int, string>>> WiredClassicDay;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredClassicWeek;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredClassicMonth;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredClassicAllTime;

        public Dictionary<int, List<KeyValuePair<int, string>>> WiredPerTeamDay;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredPerTeamWeek;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredPerTeamMonth;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredPerTeamAllTime;

        public Dictionary<int, List<KeyValuePair<int, string>>> WiredMostWinDay;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredMostWinWeek;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredMostWinMonth;
        public Dictionary<int, List<KeyValuePair<int, string>>> WiredMostWinAllTime;

        public int TradeSettings;//Default = 2;

        public RoomPromotion _promotion;

        public bool PushEnabled;
        public bool PullEnabled;
        public bool SPushEnabled;
        public bool SPullEnabled;
        public bool EnablesEnabled;
        public bool RespectNotificationsEnabled;
        public Dictionary<int, KeyValuePair<int, string>> WiredScoreBordDay;
        public Dictionary<int, KeyValuePair<int, string>> WiredScoreBordWeek;
        public Dictionary<int, KeyValuePair<int, string>> WiredScoreBordMonth;
        public List<int> WiredScoreFirstBordInformation = new List<int>();
        public bool PetMorphsAllowed;

        public void Fill(DataRow Row)
        {
            Id = Convert.ToInt32(Row["id"]);
            Name = Convert.ToString(Row["caption"]);
            Description = Convert.ToString(Row["description"]);
            Type = Convert.ToString(Row["roomtype"]);
            OwnerId = Convert.ToInt32(Row["owner"]);

            OwnerName = "";
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `username` FROM `users` WHERE `id` = @owner LIMIT 1");
                dbClient.AddParameter("owner", OwnerId);
                string result = dbClient.getString();
                if (!String.IsNullOrEmpty(result))
                    OwnerName = result;
            }

            this.Access = RoomAccessUtility.ToRoomAccess(Row["state"].ToString().ToLower());

            Category = Convert.ToInt32(Row["category"]);
            if (!string.IsNullOrEmpty(Row["users_now"].ToString()))
                UsersNow = Convert.ToInt32(Row["users_now"]);
            else
                UsersNow = 0;
            UsersMax = Convert.ToInt32(Row["users_max"]);
            ModelName = Convert.ToString(Row["model_name"]);
            Score = Convert.ToInt32(Row["score"]);
            Tags = new List<string>();
            AllowPets = Convert.ToInt32(Row["allow_pets"].ToString());
            AllowPetsEating = Convert.ToInt32(Row["allow_pets_eat"].ToString());
            RoomBlockingEnabled = Convert.ToInt32(Row["room_blocking_disabled"].ToString());
            Hidewall = Convert.ToInt32(Row["allow_hidewall"].ToString());
            Password = Convert.ToString(Row["password"]);
            Wallpaper = Convert.ToString(Row["wallpaper"]);
            Floor = Convert.ToString(Row["floor"]);
            Landscape = Convert.ToString(Row["landscape"]);
            FloorThickness = Convert.ToInt32(Row["floorthick"]);
            WallThickness = Convert.ToInt32(Row["wallthick"]);
            WhoCanMute = Convert.ToInt32(Row["mute_settings"]);
            WhoCanKick = Convert.ToInt32(Row["kick_settings"]);
            WhoCanBan = Convert.ToInt32(Row["ban_settings"]);
            chatMode = Convert.ToInt32(Row["chat_mode"]);
            chatSpeed = Convert.ToInt32(Row["chat_speed"]);
            chatSize = Convert.ToInt32(Row["chat_size"]);
            TradeSettings = Convert.ToInt32(Row["trade_settings"]);

            Group G = null;
            if (MithServer.GetGame().GetGroupManager().TryGetGroup(Convert.ToInt32(Row["group_id"]), out G))
                Group = G;
            else
                Group = null;

            foreach (string Tag in Row["tags"].ToString().Split(','))
            {
                Tags.Add(Tag);
            }

            mModel = MithServer.GetGame().GetRoomManager().GetModel(ModelName);

            this.PushEnabled = MithServer.EnumToBool(Row["push_enabled"].ToString());
            this.PullEnabled = MithServer.EnumToBool(Row["pull_enabled"].ToString());
            this.SPushEnabled = MithServer.EnumToBool(Row["spush_enabled"].ToString());
            this.SPullEnabled = MithServer.EnumToBool(Row["spull_enabled"].ToString());
            this.EnablesEnabled = MithServer.EnumToBool(Row["enables_enabled"].ToString());
            this.RespectNotificationsEnabled = MithServer.EnumToBool(Row["respect_notifications_enabled"].ToString());
            this.PetMorphsAllowed = MithServer.EnumToBool(Row["pet_morphs_allowed"].ToString());

            this.WiredScoreBoardDay = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredScoreBoardWeek = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredScoreBoardMonth = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredScoreBoardAllTime = new Dictionary<int, List<KeyValuePair<int, string>>>();

            this.WiredClassicDay = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredClassicWeek = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredClassicMonth = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredClassicAllTime = new Dictionary<int, List<KeyValuePair<int, string>>>();

            this.WiredPerTeamDay = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredPerTeamWeek = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredPerTeamMonth = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredPerTeamAllTime = new Dictionary<int, List<KeyValuePair<int, string>>>();

            this.WiredMostWinDay = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredMostWinWeek = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredMostWinMonth = new Dictionary<int, List<KeyValuePair<int, string>>>();
            this.WiredMostWinAllTime = new Dictionary<int, List<KeyValuePair<int, string>>>();
        }

        public RoomPromotion Promotion
        {
            get { return this._promotion; }
            set { this._promotion = value; }
        }

        public bool HasActivePromotion
        {
            get { return this.Promotion != null; }
        }

        public void EndPromotion()
        {
            if (!this.HasActivePromotion)
                return;

            this.Promotion = null;
        }

        public RoomModel Model
        {
            get
            {
                if (mModel == null)
                    mModel = MithServer.GetGame().GetRoomManager().GetModel(ModelName);
                return mModel;
            }
        }

    }
}