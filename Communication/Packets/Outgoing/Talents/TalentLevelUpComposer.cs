﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.HabboHotel.Talentos;

namespace Plus.Communication.Packets.Outgoing.Talents
{
    class TalentLevelUpComposer : ServerPacket
    {
        public TalentLevelUpComposer(TalentDefinition Talent)
            : base(ServerPacketHeader.TalentLevelUpMessageComposer)
        {
            base.WriteString(Talent.Type);
            base.WriteInteger(Talent.Level);
            base.WriteInteger(0);
            if (Talent.Type == "citizenship" && Talent.Level == 4)
            {
                base.WriteInteger(2);
                base.WriteString("HABBO_CLUB_VIP_7_DAYS");
                base.WriteInteger(7);
                base.WriteString(Talent.Prize);
                base.WriteInteger(0);
            }
            else
            {
                base.WriteInteger(1);
                base.WriteString(Talent.Prize);
                base.WriteInteger(0);
            }
        }
    }
}
