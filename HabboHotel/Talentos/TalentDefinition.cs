﻿using Plus.HabboHotel.Achievements;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Talentos
{

        public class TalentDefinition
        {
        GameClient Session;


        internal int Id;

         
            internal string Type;

           
            internal int ParentCategory;
        
            internal int Level;

           
            internal string AchievementGroup;

           
            internal int AchievementLevel;

           
            internal string Prize;

          
            internal int PrizeBaseItem;

         
            internal TalentDefinition(int Id, string Type, int ParentCategory, int Level, string AchId, int AchLevel, string Prize, int PrizeBaseItem)
            {
                this.Id = Id;
                this.Type = Type;
                this.ParentCategory = ParentCategory;
                this.Level = Level;
                AchievementGroup = AchId;
                AchievementLevel = AchLevel;
                this.Prize = Prize;
                this.PrizeBaseItem = PrizeBaseItem;
            }

           
            internal Achievement GetAchievement()
            {
                if (string.IsNullOrEmpty(AchievementGroup) || ParentCategory == -1)
                    return null;
           
           return MithServer.GetGame().GetAchievementManager().GetAchievement(AchievementGroup);
            }
        }
    }



