﻿using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Rooms;
using System;

namespace Plus.Communication.Packets.Incoming.TraxMachine
{
    internal class AddDiscToPlayListEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Room currentRoom = Session.GetHabbo().CurrentRoom;
            bool flag = false;//!currentRoom.CheckRights(Session);
            if (!flag)
            {
                int pId = Packet.PopInt();
                int num = Packet.PopInt();
                Item item = currentRoom.GetRoomItemHandler().GetItem(pId);
                bool flag2 = item == null;
                if (!flag2)
                {
                    bool flag3 = !currentRoom.GetTraxManager().AddDisc(item);
                    if (flag3)
                    {
                        //Session.SendMessage(new RoomNotificationComposer("", "Oops! Não foi possível adicionar o disco!", "error", "", "", true));
                        Session.SendNotification("Ha ocurrido un error al agregar ese disco");
                    }
                }
            }
        }
    }
}
