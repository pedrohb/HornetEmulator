﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.Communication.Packets.Outgoing.Rooms;
using Plus.Communication.Packets.Outgoing.Rooms.Session;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class ReloadCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_unload"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Recarga la habitación."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Session.GetHabbo().GetPermissions().HasRight("room_unload_any"))
            {
                Room R = null;
                if (!MithServer.GetGame().GetRoomManager().TryGetRoom(Room.Id, out R))
                    return;

                MithServer.GetGame().GetRoomManager().UnloadRoom(R, true);
            }
            else
            {
                if (Room.CheckRights(Session, true))
                {
                    List<RoomUser> UsersToReturn = Room.GetRoomUserManager().GetRoomUsers().ToList();
                    MithServer.GetGame().GetRoomManager().UnloadRoom(Room);
                    foreach (RoomUser User in UsersToReturn)
                    {
                        if (User == null || User.GetClient() == null)
                            continue;

                        User.GetClient().SendMessage(new RoomForwardComposer(Room.Id));
                    }
                }
            }
        }
    }
}
