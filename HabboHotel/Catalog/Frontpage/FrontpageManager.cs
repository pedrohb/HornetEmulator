﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plus.Database.Interfaces;
using log4net;

namespace Plus.HabboHotel.Catalog.Frontpage
{
    public class FrontpageManager
    {
        private static readonly ILog log = LogManager.GetLogger("Plus.HabboHotel.Catalog.FrontPage");

        private Dictionary<int, Frontpage> _frontPageData;

        public FrontpageManager()
        {
            this._frontPageData = new Dictionary<int, Frontpage>();
            this.LoadFrontPage();
        }
        
        public void LoadFrontPage()
        {
            if(this._frontPageData.Count > 0)
            {
                this._frontPageData.Clear();
            }

            using (IQueryAdapter dbQuery = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbQuery.SetQuery("SELECT * FROM `catalog_frontpage`");
                DataTable table = dbQuery.getTable();
                if(table != null)
                {
                    foreach (DataRow dataRow in table.Rows)
                    {
                        this._frontPageData.Add(Convert.ToInt32(dataRow[0]), new Frontpage((int)dataRow[0], dataRow[1].ToString(), dataRow[2].ToString(), dataRow[3].ToString()));
                    }
                }
            }
            log.Info("Catalog FrontPage Manager -> LOADED");
        }
        
        public ICollection<Frontpage> GetCatalogFrontPage()
        {
            return this._frontPageData.Values;
        }

    }
}
