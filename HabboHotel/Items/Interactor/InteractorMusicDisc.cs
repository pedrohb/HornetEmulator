﻿using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Rooms;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.TraxMachine;

namespace Plus.HabboHotel.Items.Interactor
{
    public class InteractorMusicDisc : IFurniInteractor
    {
        public void BeforeRoomLoad(Item Item)
        {
        }
        public void OnPlace(GameClient Session, Item Item)
        {
            Room room = Item.GetRoom();
            List<Item> avaliableSongs = room.GetTraxManager().GetAvaliableSongs();
            if ((avaliableSongs.Contains(Item) ? false : !room.GetTraxManager().Playlist.Contains(Item)))
            {
                avaliableSongs.Add(Item);
            }
            room.SendMessage(new LoadJukeboxUserMusicItemsComposer(avaliableSongs), false);
        }

        public void OnRemove(GameClient Session, Item Item)
        {
            Room room = Item.GetRoom();
            if ((object)Item.GetRoom().GetTraxManager().GetDiscItem(Item.Id) != (object)null)
            {
                room.GetTraxManager().StopPlayList();
                room.GetTraxManager().RemoveDisc(Item);
            }
            List<Item> avaliableSongs = room.GetTraxManager().GetAvaliableSongs();
            avaliableSongs.Remove(Item);
            room.SendMessage(new LoadJukeboxUserMusicItemsComposer(avaliableSongs), false);
        }

        public void OnTrigger(GameClient Session, Item Item, int Request, bool HasRights)
        {

        }

        public void OnWiredTrigger(Item Item)
        {

        }
    }
}