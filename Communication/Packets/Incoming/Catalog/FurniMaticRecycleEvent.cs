﻿using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.Catalog;
using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.HabboHotel.Items;
using System;
using System.Text;

namespace Plus.Communication.Packets.Incoming.Catalog
{
    class FurniMaticRecycleEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            int Diamantes = 0;
            if (Session == null || Session.GetHabbo() == null) return;
            if (!Session.GetHabbo().InRoom) return;
            if (Session.GetHabbo().Rank > 2 && !Session.GetHabbo().GetPermissions().HasRight("furnimatic_override"))
            {
                Session.SendNotification("¡Los staff no pueden usar el FurniMatic!");
                return;
            }
            var itemsCount = Packet.PopInt();
            int level = 0;
            Item item = null;
            for (int i = 0; i < itemsCount; i++)
            {
                var itemId = Packet.PopInt();
                item = Session.GetHabbo().GetInventoryComponent().GetItem(itemId);
                level = item.GetBaseItem().rare_level + level;
                using (var dbClient = MithServer.GetDatabaseManager().GetQueryReactor()) dbClient.RunQuery("DELETE FROM `items` WHERE `id` = '" + itemId + "' AND `user_id` = '" + Session.GetHabbo().Id + "' LIMIT 1");
                Session.GetHabbo().GetInventoryComponent().RemoveItem(itemId);
            }
            var reward = MithServer.GetGame().GetFurniMaticRewardsMnager().GetRandomReward(level, Session);
            if (reward == null) return;
            if (level >= 8 && level < 12)
            {
                Diamantes = 2;
            }
            else if (level >= 12 && level < 16)
            {
                Diamantes = 3;
            }
            else if (level >= 16 && level < 20)
            {
                Diamantes = 6;
            }
            else if (level >= 20 && level < 24)
            {
                Diamantes = 15;
            }
            else if (level >= 24)
            {
                Diamantes = 24;
            }
            Session.GetHabbo().Diamonds = Session.GetHabbo().Diamonds += Diamantes;
            Session.SendMessage(new HabboActivityPointNotificationComposer(Session.GetHabbo().Diamonds, Diamantes, 5));
            int rewardId;
            var furniMaticBoxId = 4692;
            ItemData data = null;
            MithServer.GetGame().GetItemManager().GetItem(furniMaticBoxId, out data);
            var maticData = DateTime.Now.Year + "-" + DateTime.Now.Month + "-" + DateTime.Now.Day;
            using (var dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("INSERT INTO `items` (`base_item`,`user_id`,`extra_data`) VALUES ('" + data.Id + "', '" + Session.GetHabbo().Id + "', @extra_data)");
                dbClient.AddParameter("extra_data", maticData);
                rewardId = Convert.ToInt32(dbClient.InsertQuery());
                dbClient.RunQuery("INSERT INTO `user_presents` (`item_id`,`base_id`,`extra_data`) VALUES ('" + rewardId + "', '" + reward.GetBaseItem().Id + "', '')");
                dbClient.RunQuery("DELETE FROM `items` WHERE `id` = " + rewardId + " LIMIT 1;");
            }

            var GiveItem = ItemFactory.CreateGiftItem(data, Session.GetHabbo(), maticData, maticData, rewardId, 0, 0);
            if (GiveItem != null)
            {
                Session.GetHabbo().GetInventoryComponent().TryAddItem(GiveItem);
                Session.SendMessage(new FurniListNotificationComposer(GiveItem.Id, 1));
                Session.SendMessage(new PurchaseOKComposer());
                Session.SendMessage(new FurniListAddComposer(GiveItem));
                Session.SendMessage(new FurniListUpdateComposer());
            }

            var response = new ServerPacket(ServerPacketHeader.FurniMaticReceiveItem);
            response.WriteInteger(1);
            response.WriteInteger(0); // received item id
            Session.SendMessage(response);
        }
    }
}