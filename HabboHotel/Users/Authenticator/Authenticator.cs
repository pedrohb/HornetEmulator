﻿using System;
using System.Data;

namespace Plus.HabboHotel.Users.Authenticator
{
    public static class HabboFactory
    {
        public static Habbo GenerateHabbo(DataRow Row, DataRow UserInfo)
        {
            return new Habbo(Convert.ToInt32(Row["id"]), Convert.ToString(Row["username"]), Convert.ToInt32(Row["rank"]), Convert.ToString(Row["motto"]), Convert.ToString(Row["look"]),
                Convert.ToString(Row["gender"]), Convert.ToInt32(Row["credits"]), Convert.ToInt32(Row["activity_points"]),
                Convert.ToInt32(Row["home_room"]), MithServer.EnumToBool(Row["block_newfriends"].ToString()), Convert.ToInt32(Row["last_online"]),
                MithServer.EnumToBool(Row["hide_online"].ToString()), MithServer.EnumToBool(Row["hide_inroom"].ToString()),
                Convert.ToDouble(Row["account_created"]), Convert.ToInt32(Row["vip_points"]), Convert.ToString(Row["machine_id"]), Convert.ToString(Row["volume"]),
                MithServer.EnumToBool(Row["chat_preference"].ToString()), MithServer.EnumToBool(Row["focus_preference"].ToString()), MithServer.EnumToBool(Row["pets_muted"].ToString()), MithServer.EnumToBool(Row["bots_muted"].ToString()),
                MithServer.EnumToBool(Row["advertising_report_blocked"].ToString()), Convert.ToDouble(Row["last_change"].ToString()), Convert.ToInt32(Row["gotw_points"]),
                MithServer.EnumToBool(Convert.ToString(Row["ignore_invites"])), Convert.ToDouble(Row["time_muted"]), Convert.ToDouble(UserInfo["trading_locked"]),
                MithServer.EnumToBool(Row["allow_gifts"].ToString()), Convert.ToInt32(Row["friend_bar_state"]), MithServer.EnumToBool(Row["disable_forced_effects"].ToString()),
                MithServer.EnumToBool(Row["allow_mimic"].ToString()), Convert.ToString(Row["talentstatus"]), Convert.ToInt32(Row["rank_vip"]),MithServer.EnumToBool(Row["can_receive_gifts"].ToString()), MithServer.EnumToBool(Row["can_receive_trade"].ToString()), MithServer.EnumToBool(Row["can_receive_follow"].ToString()));
        }
    }
}