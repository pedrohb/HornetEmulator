﻿using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;
using Plus.Database.Interfaces;
using System.Data;
using System.Text;
using System;
using Plus.Communication.Packets.Outgoing.Notifications;
using System.Linq;

using Plus.Communication.Packets.Outgoing.Rooms.Engine;


namespace Plus.HabboHotel.Rooms.Chat.Commands.Events
{
    class DeleteEventCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_delete_event"; }
        }

        public string Parameters
        {
            get { return "(message)"; }
        }

        public string Description
        {
            get { return "Envia una alerta de evento con una descripción de este."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (!Room.CheckRights(Session, true))
            {
                Session.SendWhisper("Solamente el dueño de la sala o los Staff podrán borrar este evento.");
                return;
            }
            if (Session.GetHabbo().CurrentRoom.RoomData.HasActivePromotion && !Session.GetHabbo().CurrentRoom.RoomData.Promotion.HasExpired)
            {
                using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                {
                    dbClient.RunQuery("UPDATE events_logs SET active = 0 WHERE room_id = '" + Session.GetHabbo().CurrentRoomId + "'");
                }
                Session.GetHabbo().CurrentRoom.RoomData.EndPromotion();
            }
            else
            {
                Session.SendWhisper("La sala no tiene un evento activo.");
                return;
            }
            Session.SendWhisper("El evento ha sido eliminado con éxito");
        }
    }
}
