﻿using Plus.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Rooms.Chat.Commands
{
    public class CommandList
    {
        public static DataTable Commands = null;
        public CommandList()
        {

        }
        public void LoadCommands()
        {
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                DataTable tCommmands = null;
                dbClient.SetQuery("SELECT `value`,`parametro`,`desc`,`group_id`,`subscription_id` FROM permissions_commands");
                tCommmands = dbClient.getTable();
                Commands = tCommmands;
            }
        }

        public static string ShowListCommands(int Rank, int VipRank)
        {
            StringBuilder List = new StringBuilder();
            DataRow[] MainCommands = null;
            DataRow[] VipCommands = null;
            //List.Append("Your commands:<br>");
            List.Append("Estos son los comandos que tienes disponibles:\n");
            for (int i = Rank; i > 0; i--)
            {
                List.Append("\n");
                if (i == 1 && VipRank > 0)
                {
                    for (int j = VipRank; j > 0; j--)
                    {
                        List.Append("Comandos V.I.P: " + j + "\n");
                        VipCommands = Commands.Select("subscription_id = " + j + "");
                        if (VipCommands.Count<DataRow>() > 0)
                        {
                            foreach (DataRow VipCommand in VipCommands)
                            {
                                List.Append(":" + Convert.ToString(VipCommand[0]) + " " + Convert.ToString(VipCommand[1]) + " - " + Convert.ToString(VipCommand[2]) + "\n···········································································\n");
                            }
                        }
                        else
                        {
                            List.Append("Subscripción sin comandos asignados \n");
                        }
                        List.Append("\n");
                    }

                } 
                List.Append((i > 1 ? "Rank " + i + ":\r" : ""));
                MainCommands = Commands.Select("group_id = " + i + " AND subscription_id = 0");
                if(MainCommands.Count<DataRow>() > 0)
                {
                    foreach (DataRow MainCommand in MainCommands)
                    {
                        List.Append(":" + Convert.ToString(MainCommand[0]) + " " + Convert.ToString(MainCommand[1]) + " - " + Convert.ToString(MainCommand[2]) + "\n···········································································\n");
                    }
                }
                else
                {
                    List.Append("Rango sin comandos asignados \n");
                    continue;
                }
            }
            return List.ToString();
        }
    }
}
