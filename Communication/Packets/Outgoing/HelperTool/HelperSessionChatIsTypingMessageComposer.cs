﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.HelperTool
{
    class HelperSessionChatIsTypingMessageComposer : ServerPacket
    {
        public HelperSessionChatIsTypingMessageComposer(bool Typing)
            : base(ServerPacketHeader.HelperSessionChatIsTypingMessageComposer)
        {
            base.WriteBoolean(Typing);

        }
    }
}