﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Pathfinding;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class KissCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_kiss"; }
        }

        public string Parameters
        {
            get { return "%target%"; }
        }

        public string Description
        {
            get { return "%USUARIO% - Besa al Usuario <3"; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Escribe el nombre de tu Enamorad@.");
                return;
            }

            if (!Room.PushEnabled && !Session.GetHabbo().GetPermissions().HasRight("room_override_custom_config"))
            {
                Session.SendWhisper("¡Oops! Al parecer el dueño de esta sala ha desactivado este comando.");
            }

            GameClient TargetClient = MithServer.GetGame().GetClientManager().GetClientByUsername(Params[1]);

            if (TargetClient == null)
            {
                Session.SendWhisper("¡Oops! Probablemente el usuario no se encuentre en linea.");
                return;
            }

            RoomUser TargetUser = Room.GetRoomUserManager().GetRoomUserByHabbo(TargetClient.GetHabbo().Id);

            if (TargetUser == null)
            {
                Session.SendWhisper("¡Oops! Probablemente no se encuentre en la sala");
                return;
            }

            if (TargetClient.GetHabbo().Username == Session.GetHabbo().Username)
            {
                Session.SendWhisper("¡No te Beses! :(");
                return;
            }


            if (TargetUser.isLying || TargetUser.isSitting)
            {
                Session.SendWhisper("No puedes Besarlo así...");
                return;
            }

            RoomUser ThisUser = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (ThisUser == null)
                return;

            if (!((Math.Abs(TargetUser.X - ThisUser.X) > 1) || (Math.Abs(TargetUser.Y - ThisUser.Y) > 1)))
            {
                TargetUser.ApplyEffect(9);
                ThisUser.ApplyEffect(0);
                Room.SendMessage(new ChatComposer(ThisUser.VirtualId, " *Ha Besado a " + Params[1] + "*", 0, 16));
                Room.SendMessage(new ChatComposer(TargetUser.VirtualId, "*Se ha sonrojado :$*", 0, 16));
                /*TargetUser.RotBody--;//
                TargetUser.ApplyEffect(168);
                TargetUser.Z -= 0.35;
                TargetUser.isLying = true;
                TargetUser.UpdateNeeded = true;*/
            }
            else
            {
                Session.SendWhisper("¡Oops! " + Params[1] + " esta muy alejado.");
            }
        }
    }
}