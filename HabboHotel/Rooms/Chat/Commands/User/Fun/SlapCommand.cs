﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Pathfinding;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class SlapCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_slap"; }
        }

        public string Parameters
        {
            get { return "(target)"; }
        }

        public string Description
        {
            get { return "Empuja a otro usuario."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Por favor ingresa el nombre del usuario al cual le quieres pegar una cachetada");
                return;
            }

            GameClient TargetClient = MithServer.GetGame().GetClientManager().GetClientByUsername(Params[1]);
            if (TargetClient == null)
            {
                Session.SendWhisper("Ha habido un error al momento de buscar al usuario, puede que no este conectado.");
                return;
            }

            RoomUser TargetUser = Room.GetRoomUserManager().GetRoomUserByHabbo(TargetClient.GetHabbo().Id);
            if (TargetUser == null)
            {
                Session.SendWhisper("Ha habido un error al momento de buscar al usuario, puede que no este conectado o no este en la sala.");
                return;
            }

            if (TargetClient.GetHabbo().Username == Session.GetHabbo().Username)
            {
                Session.SendWhisper("Vamos, seguro no te quieres pegar a ti mismo...");
                return;
            }

            if (TargetUser.TeleportEnabled)
            {
                Session.SendWhisper("No puedes pegarle a alguien con el modo teleport activado");
                return;
            }

            if (TargetUser.GetClient().GetHabbo().GetPermissions().HasRight("mod_tool") && !Session.GetHabbo().GetPermissions().HasRight("mod_tool"))
            {
                Session.SendWhisper("¡Vamos! Seguro no quieres pegarle a un Staff... Son fundamentales para el hotel");
                return;
            }

            if (TargetUser.Statusses.ContainsKey("lie") || TargetUser.isLying || TargetUser.RidingHorse || TargetUser.IsWalking)
            {
                Session.SendWhisper("No puedes pegarle una cachetada a alguien que esta acostado");
                return;
            }
            RoomUser ThisUser = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (ThisUser == null)
                return;

            if (!((Math.Abs(TargetUser.X - ThisUser.X) >= 2) || (Math.Abs(TargetUser.Y - ThisUser.Y) >= 2)))
            {
                GameClient Target =  MithServer.GetGame().GetClientManager().GetClientByUsername(Params[1]);
                RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Target.GetHabbo().Id);
                Room.SendMessage(new ChatComposer(ThisUser.VirtualId, "*Le pego una cachetada a " + Target.GetHabbo().Username + "*", 0, ThisUser.LastBubble));
                Room.SendMessage(new ChatComposer(TargetUser.VirtualId, "*¡Ay! Eso dolió :(*", 0, TargetUser.LastBubble));
            }
            else
            {
                Session.SendWhisper("Oops, " + Params[1] + " is not close enough!");
            }
        }
    }
}
