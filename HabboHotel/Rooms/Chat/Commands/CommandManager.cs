﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Utilities;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.GameClients;

using Plus.HabboHotel.Rooms.Chat.Commands.User;
using Plus.HabboHotel.Rooms.Chat.Commands.User.Fun;
using Plus.HabboHotel.Rooms.Chat.Commands.Moderator;
using Plus.HabboHotel.Rooms.Chat.Commands.Moderator.Fun;
using Plus.HabboHotel.Rooms.Chat.Commands.Administrator;

using Plus.Communication.Packets.Outgoing.Rooms.Chat;
using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Rooms.Chat.Commands.Events;
using Plus.HabboHotel.Items.Wired;
using Plus.HabboHotel.Rewards.Rooms.Chat.Commands.Administrator;
using Plus.HabboHotel.Rooms.Chat.Commands.Events;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using System.IO;

namespace Plus.HabboHotel.Rooms.Chat.Commands
{
    public class CommandManager
    {
        /// <summary>
        /// Command Prefix only applies to custom commands.
        /// </summary>
        private string _prefix = ":";

        /// <summary>
        /// Commands registered for use.
        /// </summary>
        private readonly Dictionary<string, IChatCommand> _commands;

        /// <summary>
        /// The default initializer for the CommandManager
        /// </summary>
        public CommandManager(string Prefix)
        {
            this._prefix = Prefix;
            this._commands = new Dictionary<string, IChatCommand>();
            this.RegisterVIP();
            this.RegisterUser();
            this.RegisterEvents();
            this.RegisterModerator();
            this.RegisterAdministrator();
        }

        /// <summary>
        /// Request the text to parse and check for commands that need to be executed.
        /// </summary>
        /// <param name="Session">Session calling this method.</param>
        /// <param name="Message">The message to parse.</param>
        /// <returns>True if parsed or false if not.</returns>
        public void UpdateTextFile(string usernaame,string mensaje, string directorioentrada)
        {
            FileStream FileST = null;
            try
            {
                FileST = new FileStream(directorioentrada, FileMode.Create, FileAccess.Write, FileShare.ReadWrite);
                using (StreamWriter tr = new StreamWriter(FileST))
                {
                    FileST = null;
                    tr.WriteLine("Comandos Disponibles ["+usernaame+"]");
                    tr.Write(mensaje);
                    tr.Close();
                }
            }
            finally
            {
                if (FileST != null)
                    FileST.Dispose();
            }
        }
        public bool Parse(GameClient Session, string Message)
        {
            if (Session == null || Session.GetHabbo() == null || Session.GetHabbo().CurrentRoom == null)
                return false;
            
            if (!Message.StartsWith(_prefix))
               return false;
            
            if (Message == _prefix + "commands")
            {
                //UpdateTextFile(Session.GetHabbo().Username,CommandList.ShowListCommands(Session.GetHabbo().Rank, Session.GetHabbo().VIPRank), "C:\\IFoster\\IFosterSWF\\gamedata\\habbopages\\commands.txt");
                Session.SendMessage(new MOTDNotificationComposer(CommandList.ShowListCommands(Session.GetHabbo().Rank,Session.GetHabbo().VIPRank)));
                //Session.SendMessage(new NuxNotificationComposer(4));
                StringBuilder List = new StringBuilder();
                List.Append("Estos son los comandos que tienes disponibles:\n");
                foreach (var CmdList in _commands.ToList())
                {
                    if (!string.IsNullOrEmpty(CmdList.Value.PermissionRequired))
                    {
                        if (!Session.GetHabbo().GetPermissions().HasCommand(CmdList.Value.PermissionRequired))
                            continue;
                    }

                    List.Append(":" + CmdList.Key + " " + CmdList.Value.Parameters + " - " + CmdList.Value.Description + "\n");
                }
                // Session.SendMessage(new MOTDNotificationComposer(List.ToString()));
                return true;
            }

            Message = Message.Substring(1);
            string[] Split = Message.Split(' ');

            if (Split.Length == 0)
                return false;

            IChatCommand Cmd = null;
            if (_commands.TryGetValue(Split[0].ToLower(), out Cmd))
            {
                if (Session.GetHabbo().GetPermissions().HasRight("mod_tool"))
                    this.LogCommand(Session.GetHabbo().Id, Message, Session.GetHabbo().MachineId);

                if (!string.IsNullOrEmpty(Cmd.PermissionRequired))
                {
                    if (!Session.GetHabbo().GetPermissions().HasCommand(Cmd.PermissionRequired))
                        return false;
                }


                Session.GetHabbo().IChatCommand = Cmd;
                Session.GetHabbo().CurrentRoom.GetWired().TriggerEvent(WiredBoxType.TriggerUserSaysCommand, Session.GetHabbo(), this);

                Cmd.Execute(Session, Session.GetHabbo().CurrentRoom, Split);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Registers the VIP set of commands.
        /// </summary>
        private void RegisterVIP()
        {
            this.Register("spull", new SuperPullCommand());
        }

        /// <summary>
        /// Registers the Events set of commands.
        /// </summary>
        private void RegisterEvents()
        {
            this.Register("event", new EventAlertCommand());
            this.Register("goevent", new GoEventCommand());
            this.Register("ge", new GoEventCommand());
            this.Register("devent", new DeleteEventCommand());
            this.Register("diamond", new GiveDiamondCommand());
            this.Register("eventcount", new EventCountCommand());
            this.Register("lastevents", new LastEvents());
            // this.Register("addevent", new Addevent());
        }

        /// <summary>
        /// Registers the default set of commands.
        /// </summary>
        private void RegisterUser()
        {
            this.Register("about", new InfoCommand());
            this.Register("pickall", new PickAllCommand());
            this.Register("ejectall", new EjectAllCommand());
            this.Register("lay", new LayCommand());
            this.Register("sit", new SitCommand());
            this.Register("color", new ColourCommand());
            this.Register("nux", new CommandNux());
            this.Register("stand", new StandCommand());
            this.Register("skiss", new KissCommand());
            this.Register("mutepets", new MutePetsCommand());
            this.Register("mutebots", new MuteBotsCommand());

            this.Register("selfie", new SelfieCommand());
            this.Register("smoke", new SmokeCommand());
            this.Register("slap", new SlapCommand());
            this.Register("burn", new BurnCommand());

            this.Register("copy", new MimicCommand());
            this.Register("dance", new DanceCommand());
            this.Register("push", new PushCommand());
            this.Register("pull", new PullCommand());
            this.Register("enable", new EnableCommand());
            this.Register("follow", new FollowCommand());
            this.Register("faceless", new FacelessCommand());
            this.Register("moonwalk", new MoonwalkCommand());

            this.Register("reload", new ReloadCommand());
            this.Register("fixroom", new RegenMaps());
            this.Register("empty", new EmptyItems());
            this.Register("setmax", new SetMaxCommand());
            this.Register("setspeed", new SetSpeedCommand());
            //this.Register("disablefriends", new DisableFriendsCommand());
            //this.Register("enablefriends", new EnableFriendsCommand());
            this.Register("disablediagonal", new DisableDiagonalCommand());
            this.Register("flagme", new FlagMeCommand());

            this.Register("stats", new StatsCommand());
            this.Register("kickpets", new KickPetsCommand());
            this.Register("kickbots", new KickBotsCommand());

            this.Register("room", new RoomCommand());
            //this.Register("dnd", new DNDCommand());
            this.Register("kill", new KillCommand());
            this.Register("disablegifts", new DisableGiftsCommand());
            this.Register("convertcredits", new ConvertCreditsCommand());
            this.Register("personal", new PersonalCommand());
            this.Register("pet", new PetCommand());
            this.Register("spush", new SuperPushCommand());
            this.Register("superpush", new SuperPushCommand());
            this.Register("ayuda", new HelpCommand());
            this.Register("convertdiamonds", new ConvertDiamondsCommand());

        }

        /// <summary>
        /// Registers the moderator set of commands.
        /// </summary>
        private void RegisterModerator()
        {
            this.Register("userson", new UsersOnCommand());
            this.Register("ban", new BanCommand());
            this.Register("mip", new MIPCommand());
            this.Register("ipban", new IPBanCommand());
            this.Register("viewclone", new ViewCloneCommand());
            this.Register("addword", new AddWord());
            this.Register("ui", new UserInfoCommand());
            this.Register("helpcount", new HelpCount());
            this.Register("userinfo", new UserInfoCommand());
            this.Register("roomcredits", new GiveRoom());
            this.Register("sa", new StaffAlertCommand());
            this.Register("roomunmute", new RoomUnmuteCommand());
            this.Register("roommute", new RoomMuteCommand());
            this.Register("roombadge", new RoomBadgeCommand());
            this.Register("roomalert", new RoomAlertCommand());
            this.Register("roomkick", new RoomKickCommand());
            this.Register("mute", new MuteCommand());
            this.Register("smute", new MuteCommand());
            this.Register("unmute", new UnmuteCommand());
            this.Register("massbadge", new MassBadgeCommand());
            this.Register("poll", new  QuickPollCommand());
            this.Register("addpoll", new addpoolCommand());
            this.Register("massgive", new MassGiveCommand());
            this.Register("globalgive", new GlobalGiveCommand());
            this.Register("kick", new KickCommand());
            this.Register("skick", new KickCommand());
            this.Register("ha", new HotelAlertCommand());
            this.Register("hotelalert", new HotelAlertCommand());
            this.Register("hal", new HALCommand());
            this.Register("give", new GiveCommand());
            this.Register("givebadge", new GiveBadgeCommand());
            this.Register("dc", new DisconnectCommand());
            // this.Register("kill", new DisconnectCommand());
            this.Register("disconnect", new DisconnectCommand());
            this.Register("alert", new AlertCommand());

            this.Register("tradeban", new TradeBanCommand());

            this.Register("tele", new TeleportCommand());
            this.Register("summon", new SummonCommand());
            this.Register("get", new SummonCommand());
            this.Register("override", new OverrideCommand());
            this.Register("massenable", new MassEnableCommand());
            this.Register("massdance", new MassDanceCommand());
            this.Register("freeze", new FreezeCommand());
            this.Register("unfreeze", new UnFreezeCommand());
            this.Register("fastwalk", new FastwalkCommand());
            this.Register("superfastwalk", new SuperFastwalkCommand());
            this.Register("coords", new CoordsCommand());
            this.Register("alleyesonme", new AllEyesOnMeCommand());
            this.Register("allaroundme", new AllAroundMeCommand());
            this.Register("forcesit", new ForceSitCommand());

            this.Register("ignorewhispers", new IgnoreWhispersCommand());
            this.Register("forcedeffects", new DisableForcedFXCommand());

            this.Register("say", new MakeSayCommand());
            this.Register("flaguser", new FlagUserCommand());
            this.Register("staffon", new StaffOnCommand());
        }

        /// <summary>
        /// Registers the administrator set of commands.
        /// </summary>
        private void RegisterAdministrator()
        {
            this.Register("updatejukebox", new UpdateJukeBox());
            this.Register("item", new EditFurniture());
            this.Register("bubble", new BubbleCommand());
            this.Register("update", new UpdateCommand());
            this.Register("emptyuser", new EmptyUser());
            this.Register("deletegroup", new DeleteGroupCommand());
            this.Register("handitem", new CarryCommand());
            this.Register("goto", new GOTOCommand());
        }

        /// <summary>
        /// Registers a Chat Command.
        /// </summary>
        /// <param name="CommandText">Text to type for this command.</param>
        /// <param name="Command">The command to execute.</param>
        public void Register(string CommandText, IChatCommand Command)
        {
            this._commands.Add(CommandText, Command);
        }

        public static string MergeParams(string[] Params, int Start)
        {
            var Merged = new StringBuilder();
            for (int i = Start; i < Params.Length; i++)
            {
                if (i > Start)
                    Merged.Append(" ");
                Merged.Append(Params[i]);
            }

            return Merged.ToString();
        }

        public void LogCommand(int UserId, string Data, string MachineId)
        {
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("INSERT INTO `logs_client_staff` (`user_id`,`data_string`,`machine_id`, `timestamp`) VALUES (@UserId,@Data,@MachineId,@Timestamp)");
                dbClient.AddParameter("UserId", UserId);
                dbClient.AddParameter("Data", Data);
                dbClient.AddParameter("MachineId", MachineId);
                dbClient.AddParameter("Timestamp", MithServer.GetUnixTimestamp());
                dbClient.RunQuery();
            }
        }

        public bool TryGetCommand(string Command, out IChatCommand IChatCommand)
        {
            return this._commands.TryGetValue(Command, out IChatCommand);
        }
    }
}