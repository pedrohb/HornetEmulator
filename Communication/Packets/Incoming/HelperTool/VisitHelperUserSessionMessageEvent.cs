﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.HelperTool;
using Plus.HabboHotel.Guides;

namespace Plus.Communication.Packets.Incoming.HelperTool
{
    class VisitHelperUserSessionMessageEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            GuideTour tour = MithServer.GetGame().GetGuideManager().getGuideTourByHelper(Session.GetHabbo());
            if (tour != null)
            {
                Session.SendMessage(new HelperSessionVisiteRoomMessageComposer(tour.getNoob().CurrentRoom));
            }


        }
    }
}
