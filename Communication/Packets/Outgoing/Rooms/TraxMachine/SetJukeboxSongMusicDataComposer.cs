﻿using Plus.HabboHotel.Rooms.TraxMachine;
using System;
using System.Collections.Generic;

namespace Plus.Communication.Packets.Outgoing.TraxMachine
{
    internal class SetJukeboxSongMusicDataComposer : ServerPacket
    {
        public SetJukeboxSongMusicDataComposer(ICollection<TraxMusicData> Songs) : base(ServerPacketHeader.SetJukeboxSongMusicDataMessageComposer)
        {
            base.WriteInteger(Songs.Count);
            foreach (TraxMusicData current in Songs)
            {
                base.WriteInteger(current.Id);
                base.WriteString(current.CodeName);
                base.WriteString(current.Name);
                base.WriteString(current.Data);
                base.WriteInteger(checked((int)(unchecked((double)current.Length * 1000.0))));
                base.WriteString(current.Artist);
            }
        }
        public SetJukeboxSongMusicDataComposer(int musicid, string codename, string songname, string data, int length, string artist) : base(ServerPacketHeader.SetJukeboxSongMusicDataMessageComposer)
        {
            base.WriteInteger(1);
            base.WriteInteger(musicid);
            base.WriteString(codename);
            base.WriteString(songname);
            base.WriteString(data);
            base.WriteInteger(checked((int)(unchecked((double)length * 1000.0))));
            base.WriteString(artist);
        }
    }
}
