﻿using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;
using Plus.Database.Interfaces;
using System.Data;
using System.Text;
using System;
using Plus.Communication.Packets.Outgoing.Notifications;
using System.Linq;

using Plus.Communication.Packets.Outgoing.Rooms.Engine;
using Plus.Communication.Packets.Outgoing.Inventory.Purse;
using Plus.Communication.Packets.Outgoing.Messenger;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Events
{
    class GiveDiamondCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_give_1diamond"; }
        }

        public string Parameters
        {
            get { return "(message)"; }
        }

        public string Description
        {
            get { return "Envia una alerta de evento con una descripción de este."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Por favor escribe el nombre del usuario.");
                return;
            }
            if (Session.GetHabbo().Rank > 9)
            {
                Session.SendWhisper("Solo los CEO's pueden usar este comando");
                return;
            }
            string Username = Params[1];
            GameClient TargetClient = MithServer.GetGame().GetClientManager().GetClientByUsername(Username);

            if(Session.GetHabbo() == TargetClient.GetHabbo())
            {
                Session.SendWhisper("¿Que intentas?");
                return;
            }

            if(TargetClient == null)
            {
                Session.SendWhisper("El usuario no existe o no está conectado.");
                return;
            }

            TargetClient.GetHabbo().Diamonds += 1;
            TargetClient.SendMessage(new HabboActivityPointNotificationComposer(TargetClient.GetHabbo().Diamonds, 1, 5));
            Session.SendWhisper("Le has dado exitosamente 1 diamante al usuario: "+TargetClient.GetHabbo().Username);
            MithServer.GetGame().GetClientManager().StaffAlert(new NewConsoleMessageComposer(0x7fffffff, "SYSTEM: El usuario: " + Session.GetHabbo().Username + " le ha dado 1 diamante al usuario: "+TargetClient.GetHabbo().Username, 0, 0x7fffffff));
        }
    }
}
