﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;
using Plus.HabboHotel.Users;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class ColourCommand : IChatCommand
    {

        public string PermissionRequired
        {
            get { return "command_colour"; }
        }
        public string Parameters
        {
            get { return ""; }
        }
        public string Description
        {
            get { return "off/red/orange/yellow/green/blue/purple/pink"; }
        }
        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Oops, debes elegir el color que piensas usar!");
                return;
            }
            string chatColour = Params[1];
            string Colour = chatColour.ToUpper();
            switch (chatColour)
            {
                case "none":
                case "black":
                case "off":
                    Session.GetHabbo().chatColour = "";
                    Session.SendWhisper("Tu Color de Chat Ha Sido Desactivado");
                    break;
                case "blue":
                case "red":
                case "orange":
                case "yellow":
                case "green":
                case "purple":
                case "pink":
                    Session.GetHabbo().chatColour = chatColour;
                    Session.SendWhisper("Tu Color de chat ha sido activado a : " + Colour + "");
                    break;
                default:
                    Session.SendWhisper("El Color de chat: " + Colour + " No Existe!");
                    break;
            }
            return;
            }
        }
    }