﻿using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.HelperTool
{
    class ErrorHelpRequestComposer : ServerPacket
    {
        public ErrorHelpRequestComposer(int i, GameClient Session)
            : base(ServerPacketHeader.CallForHelperErrorMessageComposer)
        {
            base.WriteInteger(i);//Count
            
        }
    }
}