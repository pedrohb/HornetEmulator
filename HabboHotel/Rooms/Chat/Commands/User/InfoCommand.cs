﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Plus.Communication.Packets.Outgoing.Users;
using Plus.Communication.Packets.Outgoing.Notifications;


using Plus.Communication.Packets.Outgoing.Handshake;
using Plus.Communication.Packets.Outgoing.Quests;
using Plus.HabboHotel.Items;
using Plus.Communication.Packets.Outgoing.Inventory.Furni;
using Plus.Communication.Packets.Outgoing.Catalog;
using Plus.HabboHotel.Quests;
using Plus.HabboHotel.Rooms;
using System.Threading;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Avatar;
using Plus.Communication.Packets.Outgoing.Pets;
using Plus.Communication.Packets.Outgoing.Messenger;
using Plus.HabboHotel.Users.Messenger;
using Plus.Communication.Packets.Outgoing.Rooms.Polls;
using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Availability;
using Plus.Communication.Packets.Outgoing;
using Plus.Communication.Packets.Outgoing.Rooms.Polls.Questions;
using Plus.Database.Interfaces;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User
{
    class InfoCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_info"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Información acerca del servidor."; }
        }

        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            TimeSpan Uptime = DateTime.Now - MithServer.ServerStarted;
            int OnlineUsers = MithServer.GetGame().GetClientManager().Count;
            int RoomCount = MithServer.GetGame().GetRoomManager().Count;
            DataRow Items = null, rooms = null, users = null;
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT count(id) FROM users");
                users = dbClient.getRow();
                dbClient.SetQuery("SELECT count(id) FROM users WHERE DATE(FROM_UNIXTIME(account_created)) = CURDATE()");
                Items = dbClient.getRow();
                dbClient.SetQuery("SELECT count(id) FROM rooms");
                rooms = dbClient.getRow();
            }
            StringBuilder information = new StringBuilder();
            information.Append("<font color=\"#DF7401\" size=\"30\"><b>Hornet Server</b></font><br>");
            information.Append("<font color=\"#1C1C1C\" size=\"10\">              Emulation of Habbo hotel<br><br></font>");
            information.Append("<b>Descripcion:</b><br>");
            information.Append("Hornet es un emulador capaz de realizar las funciones básicas de habbohotel y varias funciones únicas desarrolladas por InstaServ. Basado en el original Butterfly y Plusemu<br><br>");
            information.Append("<b>Estadísticas del Hotel:</b><br>");
            information.Append("Usuarios en Línea: <b><font color = \"#04B404\" >" + OnlineUsers + "</font></b><br>");
            information.Append("Salas cargadas: <b><font color = \"#04B404\" >" + RoomCount + "</font></b><br>");
            information.Append("Usuarios registrados: <b><font color = \"#04B404\" >" + users[0] + "</font></b><br>");
            information.Append("Salas creadas: <b><font color = \"#04B404\" >" + rooms[0] + "</font></b><br>");
            information.Append("Usuarios registrados hoy: <b><font color = \"#DF0101\" > " + Items[0] + "</font></b><br><br>");
            information.Append("<b>Créditos:</b><br>");
            information.Append("- <font color=\"#FF8000\"><b>InstaServ</b></font><br>");
            information.Append("- <font color=\"#FF8000\"><b>Meth0d.</b></font><br>");
            information.Append("- <font color=\"#FF8000\"><b>Sleedmore</b></font><br><br>");
            information.Append("<b>Licencia:</b><br>");
            information.Append("<font color=\"#848484\" size=\"14\">InstaServ Community</font>");
            Session.SendMessage(new RoomNotificationComposer("Información del Hotel",information.ToString(), "hornetlogo", "Ok", "event:"));
            //Session.SendMessage(new RoomNotificationComposer("Powered by BlueSky EMULATOR",
            //     "<font color = \"#0000FF\" ><b>Créditos</b>:</font>\n" +
            //     "<font color = \"#0080FF\" ><b>Distorsion</b></font> (Desarrollador de BlueSky Emulator)\n" +
            //     "<font color = \"#FA58F4\" ><b>Juan</b></font>  (Desarrollador de la CMS & SWF de DEBBUX)\n" +
            //     "Desarrolladores de PlusEmu\n" +
            //     "Desarrolladores de ButterFly Emulator\n\n" +
            //     "<font color = \"#0000FF\" ><b>Estadísticas del hotel:</b>:</font>\n" +
            //     "Usuarios en Línea: <b><font color = \"#04B404\" >" + OnlineUsers + "</font></b>\n" +
            //     "Salas cargadas: <b><font color = \"#04B404\" >" + RoomCount + "</font></b>\n" +
            //     "Usuarios Registrados: <b><font color = \"#04B404\" >" + users[0] + "</font></b>\n" +
            //     "Salas creadas: <b><font color = \"#04B404\" >" + rooms[0] + "</font></b>\n" +
            //     "Usuarios registrados hoy: <b><font color = \"#DF0101\" > " + Items[0] + "</font></b>\n" +
            //     "Tiempo en línea: <font color = \"#04B404\" ><b> " + Uptime.Days + "</b></font> día(s), <font color = \"#04B404\" ><b>" + Uptime.Hours + "</b></font> hora(s) y <font color = \"#04B404\" ><b> " + Uptime.Minutes + "</b></font> minuto(s).\n\n" +
            //     "<font color = \"#0000FF\" ><b>RELEASE</b>:</font>\n" + MithServer.SWFRevision, "emupix", ""));

        }

    }
}