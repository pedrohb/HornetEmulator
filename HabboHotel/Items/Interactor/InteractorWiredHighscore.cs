﻿using System;
using Plus.HabboHotel.GameClients;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Items.Interactor
{
    class InteractorWiredHighscore : IFurniInteractor
    {
        public void BeforeRoomLoad(Item Item)
        {
        }
        public void OnPlace(GameClient Session, Item Item)
        {
            Item.GetRoom().GetRoomItemHandler().wf_highscore_count++;
            Item.Scores.Clear();
            Item.GetRoom().GetRoomItemHandler().GetnVerify(Item);

            Item.ExtraData = (Item.ExtraData == "1" && Item.ExtraData != null && Item.ExtraData != "") ? "1" : "0";
            Item.UpdateState(true, true);
            Item.UpdateNeeded = true;
        }

        public void OnRemove(GameClient Session, Item Item)
        {
            Item.GetRoom().GetRoomItemHandler().wf_highscore_count--;

            Item.ExtraData = "0";
            Item.UpdateState(true, true);
            Item.UpdateNeeded = true;
        }

        public void OnTrigger(GameClient Session, Item Item, int Request, bool HasRights)
        {

            if (!HasRights)
            {
                return;
            }
            Item.Scores.Clear();
            Item.GetRoom().GetRoomItemHandler().GetnVerify(Item);

            if (Item.ExtraData == "1" && Item.ExtraData != "" && Item.ExtraData != null)
            {
                Item.ExtraData = "0";
                Item.UpdateState(true, true);
                Item.RequestUpdate(4, true);
            }
            else if (Item.ExtraData == "0" || Item.ExtraData == "" || Item.ExtraData == null)
            {
                Item.ExtraData = "1";
                Item.UpdateState(true, true);
                Item.RequestUpdate(4, true);
            }
        }

        public void OnWiredTrigger(Item Item)
        {
        }
    }
}
