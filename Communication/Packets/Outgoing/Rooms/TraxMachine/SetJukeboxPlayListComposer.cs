﻿using Plus.HabboHotel.Items;
using Plus.HabboHotel.Rooms;
using System;
using System.Collections.Generic;

namespace Plus.Communication.Packets.Outgoing.TraxMachine
{
    internal class SetJukeboxPlayListComposer : ServerPacket
    {
        public SetJukeboxPlayListComposer(Room room) : base(ServerPacketHeader.SetJukeboxPlayListMessageComposer)
        {
            List<Item> playlist = room.GetTraxManager().Playlist;
            base.WriteInteger(playlist.Count);
            base.WriteInteger(playlist.Count);
            foreach (Item current in playlist)
            {
                int i;
                int.TryParse(current.ExtraData, out i);
                base.WriteInteger(current.Id);
                base.WriteInteger(i);
            }
        }
    }
}
