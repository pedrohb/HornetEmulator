﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Plus.HabboHotel.Users;
using Plus.HabboHotel.GameClients;

using Plus.Database.Interfaces;
using Plus.Communication.Packets.Outgoing.Notifications;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class EventCountCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_eventcount"; }
        }

        public string Parameters
        {
            get { return "(username)"; }
        }

        public string Description
        {
            get { return "Muestra toda la información de un usuario."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            DataTable Tabla = null;
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT count(events_logs.id), users.username, events_logs.fecha FROM events_logs INNER JOIN users ON events_logs.user_id = users.id WHERE events_logs.fecha LIKE '" + DateTime.Today.ToString("yyyy-MM-dd") + "%' GROUP BY users.username ORDER BY events_logs.fecha ASC");
                Tabla = dbClient.getTable();
            }
            StringBuilder Lista = new StringBuilder();
            Lista.Append("Logs de eventos para el día " + DateTime.Today.ToString("yyyy/MM/dd") + ": \r\r");
            foreach (DataRow Row in Tabla.Rows)
            {
                Lista.Append("Usuario: " + Convert.ToString(Row[1]) + " - Eventos abiertos: " + Convert.ToString(Row[0]) + "\r\r");
            }
            Session.SendMessage(new MOTDNotificationComposer(Lista.ToString()));
            return;
        }
    }
}