﻿using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Rooms.TraxMachine;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Plus.Communication.Packets.Outgoing.Moderation;
namespace Plus.HabboHotel.Items.Interactor
{
    internal class InteractorJukebox : IFurniInteractor
    {
        public  void BeforeRoomLoad(Item Item)
        {
            if (Item.ExtraData == "0")
            {
                if (Item.GetRoom().GetTraxManager().IsPlaying)
                {
                    Item.GetRoom().GetTraxManager().PlayPlaylist();
                }
            }
        }

        public void OnPlace(GameClient Session, Item Item)
        {
            Item.GetRoom().GetRoomItemHandler().JukeBoxCount++;         
        }

        public void OnRemove(GameClient Session, Item Item)
        {
            Item.GetRoom().GetRoomItemHandler().JukeBoxCount--;
            Item.GetRoom().GetTraxManager().ClearPlayList();
            Item.ExtraData = "0";
            Item.UpdateState();
        }

        public void OnTrigger(GameClient Session, Item Item, int Request, bool HasRights)
        {
            Room room = Item.GetRoom();
            if ((Request == 0 ? true : Request == 1))
            {
                room.GetTraxManager().TriggerPlaylistState();
            }
        }
            
        public void OnWiredTrigger(Item Item)
        {
            if (!Item.GetRoom().GetTraxManager().IsPlaying)
            {
                Item.GetRoom().GetTraxManager().PlayPlaylist();
            }
            else
            {
                Item.GetRoom().GetTraxManager().StopPlayList();
            }
        }
    }
}