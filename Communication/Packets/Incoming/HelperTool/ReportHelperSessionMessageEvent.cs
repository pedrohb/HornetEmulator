﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.HelperTool;
using Plus.HabboHotel.Guides;
using Plus.HabboHotel.Users;
using Plus.Communication.Packets.Outgoing.Notifications;

namespace Plus.Communication.Packets.Incoming.HelperTool
{
    class ReportHelperSessionMessageEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            string message = Packet.PopString();
            //reportype = 5
            GuideTour tour = MithServer.GetGame().GetGuideManager().getGuideTourByHabbo(Session.GetHabbo());

            if (tour != null)
            {
                Habbo reported = tour.getHelper();

                if (reported == Session.GetHabbo())
                {
                    reported = tour.getNoob();
                }
                MithServer.GetGame().GetModerationTool().SendNewTicket(Session, 5, reported.Id, message, null);
                MithServer.GetGame().GetClientManager().StaffAlert(new MOTDNotificationComposer("Nuevo reporte, tipo de reporte: [Herramienta Alfa] "));

                MithServer.GetGame().GetGuideManager().endSession(tour);



            }
        }
    }
}
