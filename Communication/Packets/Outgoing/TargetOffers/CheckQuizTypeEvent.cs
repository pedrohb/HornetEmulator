﻿using Plus.Communication.Packets.Incoming;

namespace Plus.Communication.Packets
{
    public class CheckQuizTypeEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            MithServer.GetGame().GetAchievementManager().ProgressAchievement(Session, "ACH_SafetyQuizGraduate", 1, false);

        }
    }
}