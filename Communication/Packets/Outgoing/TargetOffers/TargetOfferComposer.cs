﻿using Plus.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plus.HabboHotel.Global;
using Plus.HabboHotel.TargetOffers;
namespace Plus.Communication.Packets.Outgoing.TargetOffers
{
    class TargetOfferComposer : ServerPacket
    {
        public TargetOfferComposer(int UserCountBuy, string itemName, string OfferTitle, string OfferDescription, int OfferCreditsPrice, int OfferDiamondPrice, int OfferTime, int OfferMaxBuy, string OfferBadgeReward, string OfferIconImage, string OfferImage) : base(ServerPacketHeader.TargetOfferComposer)
        {
            base.WriteInteger(1);
            base.WriteInteger(190);
            base.WriteString("bf16_tko_gr1");
            base.WriteString("bf16_tko1");
            base.WriteInteger(OfferCreditsPrice); //INT donde te da el valor de la oferta de los creditos
            base.WriteInteger(OfferDiamondPrice); //Diamonds lo mismo pero en diamantes
            base.WriteInteger(5); // 5
            base.WriteInteger(OfferMaxBuy - UserCountBuy); // cantidad de furnis
            base.WriteInteger(OfferTime); //3 Days ... time in seconds 
            base.WriteString(OfferTitle); //Title 
            base.WriteString(OfferDescription); //Description 
            base.WriteString("targetedoffers/" + OfferImage); //Image Large 
            base.WriteString("targetedoffers/" + OfferIconImage); //Image on Close Notification 
            base.WriteInteger(1);
            base.WriteInteger(15); // conteo de mega oferta
            base.WriteString(string.Empty); //1 Month HC 2MONTH HC_2_MONTH_INTERNAL 3HC HC_3_MONTH_INTERNAL *y aqui cuando compro la oferta me da hc, depende lo que yo ponga aqui, si 1 mes, 2 meses, o 3 meses*
            base.WriteString(itemName); //Snack //furni
            base.WriteString(string.Empty); //10 Credits //reward 
            base.WriteString(string.Empty); //Roof Building 
            base.WriteString(string.Empty); //Snack 
            base.WriteString(string.Empty); //10 Credits 
            base.WriteString(string.Empty); //10 Credits 
            base.WriteString(string.Empty); //Building 1 
            base.WriteString(string.Empty); //Building 2 
            base.WriteString(string.Empty); //10 Credits 
            base.WriteString(string.Empty); //Clothes Scarf 
            base.WriteString(string.Empty); //10 Credits 
            base.WriteString(string.Empty); //10 Credits 
            base.WriteString(OfferBadgeReward); // Y esto es la placa, que por ejemplo si pongo ADM cuando la compro me da la placa ADM
            base.WriteString(string.Empty); //10 Credits  
        }
    }
}