﻿using Plus.Communication.Packets.Outgoing.Rooms.Chat;
using Plus.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class PersonalCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_dnd"; }
        }

        public string Parameters
        {
            get { return "staff/trade/friends/gifts/music/staffchat"; }
        }

        public string Description
        {
            get { return "Configura tu usuario"; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Vaya, debes elegir una opción de usuario");
                return;
            }

            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User == null)
                return;

            if (!Room.CheckRights(Session, true))
            {
                return;
            }
            string Options = Params[1];
            switch (Options)
            {
                case "staff":
                    {
                        if(Session.GetHabbo().Rank > 1 && Session.GetHabbo().Rank != 2)
                        {
                            if (Session.GetHabbo().Effects().CurrentEffect == 102)
                            {
                                Session.GetHabbo().Effects().ApplyEffect(0);
                                Session.GetHabbo().GetClient().SendMessage(new WhisperComposer(User.VirtualId, "¡Has deshabilitado el efecto staff!", 0, 34));
                            }
                            else
                            {
                                Session.GetHabbo().Effects().ApplyEffect(102);
                                Session.GetHabbo().GetClient().SendMessage(new WhisperComposer(User.VirtualId, "¡Has habilitado el efecto staff!", 0, 34));
                            }
                        }
                        else if(Session.GetHabbo().Rank == 2)
                        {
                            if (Session.GetHabbo().Effects().CurrentEffect == 178)
                            {
                                Session.GetHabbo().Effects().ApplyEffect(0);
                                Session.GetHabbo().GetClient().SendMessage(new WhisperComposer(User.VirtualId, "¡Has deshabilitado el efecto embajador!", 0, 34));
                            }
                            else
                            {
                                Session.GetHabbo().Effects().ApplyEffect(178);
                                Session.GetHabbo().GetClient().SendMessage(new WhisperComposer(User.VirtualId, "¡Has habilitado el efecto embajador!", 0, 34));
                            }
                        }
                        break;        
                    }
                case "friends":
                    {

                        Session.GetHabbo().AllowFriendRequests = !Session.GetHabbo().AllowFriendRequests;
                        
                        using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `users` SET `block_newfriends` = @FriendsEnabled WHERE `id` = '" + Session.GetHabbo().Id + "'");
                            dbClient.AddParameter("FriendsEnabled", MithServer.BoolToEnum(Session.GetHabbo().AllowFriendRequests));
                            dbClient.RunQuery();
                        }
                        Session.GetHabbo().GetClient().SendMessage(new WhisperComposer(User.VirtualId, "¡Has " + (Session.GetHabbo().AllowFriendRequests == true ? "" : "des") + "habilitado las peticiones de amigos!", 0, 34));
                        break;
                    }
                case "trade":
                    {
                        Session.GetHabbo().CanReceiveTrade = !Session.GetHabbo().CanReceiveTrade;
                        using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `users` SET `can_receive_trade` = @CanReceiveTrade WHERE `id` = @userId LIMIT 1");
                            dbClient.AddParameter("CanReceiveTrade", MithServer.BoolToEnum(Session.GetHabbo().CanReceiveTrade));
                            dbClient.AddParameter("userId", Session.GetHabbo().Id);
                            dbClient.RunQuery();
                        }
                        
                        Session.GetHabbo().GetClient().SendMessage(new WhisperComposer(User.VirtualId, "¡Has " + (Session.GetHabbo().CanReceiveTrade == true ? "" : "des") + "habilitado tu tradeo!", 0, 34));
                        break;
                    }
                case "gifts":
                    {
                        Session.GetHabbo().AllowGifts = !Session.GetHabbo().AllowGifts;
                        using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `users` SET `allow_gifts` = @AllowGifts WHERE `id` = '" + Session.GetHabbo().Id + "'");
                            dbClient.AddParameter("AllowGifts", MithServer.BoolToEnum(Session.GetHabbo().AllowGifts));
                            dbClient.RunQuery();
                        }
                        Session.GetHabbo().GetClient().SendMessage(new WhisperComposer(User.VirtualId, "¡Has " + (Session.GetHabbo().AllowGifts == true ? "" : "des") + "habilitado los regalos de terceros!", 0, 34));
                        break;
                    }
                case "staffchat":
                    {
                        if (Session.GetHabbo().StaffChatDisable)
                        {
                            Session.GetHabbo().StaffChatDisable = false;
                            
                        }
                        else
                        {
                            Session.GetHabbo().StaffChatDisable = true;
                        }
                        Session.GetHabbo().GetClient().SendMessage(new WhisperComposer(User.VirtualId, "¡Has " + (Session.GetHabbo().StaffChatDisable == true ? "des" : "") + "habilitado el modo chat staff", 0, 34));
                        break;
                    }
                case "follow":
                    {
                        Session.GetHabbo().CanReceiveFollow = !Session.GetHabbo().CanReceiveFollow;
                        using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                        {
                            dbClient.SetQuery("UPDATE `users` SET `can_receive_follow` = @CanReceiveFollow WHERE `id` = '" + Session.GetHabbo().Id + "'");
                            dbClient.AddParameter("CanReceiveFollow", MithServer.BoolToEnum(Session.GetHabbo().CanReceiveFollow));
                            dbClient.RunQuery();
                        }
                        Session.GetHabbo().GetClient().SendMessage(new WhisperComposer(User.VirtualId, "¡Has " + (Session.GetHabbo().CanReceiveFollow == true ? "" : "des") + "habilitado la opción que te puedan seguir!", 0, 34));
                        break;
                    }
                case "music":
                    {
                        Session.GetHabbo().ClientVolume.Add(0);
                        Session.GetHabbo().GetClient().SendMessage(new WhisperComposer(User.VirtualId, "Has bajado el volumen del cliente", 0, 34));
                        break;
                    }
                case "console":
                    {
                        Session.GetHabbo().AllowConsoleMessages = !Session.GetHabbo().AllowConsoleMessages;
                        Session.SendWhisper("¡Has " + (Session.GetHabbo().AllowConsoleMessages == true ? "" : "des") + "habilitado los mensajes de consola!.");
                        break;
                    }
                default:
                    Session.GetHabbo().GetClient().SendMessage(new WhisperComposer(User.VirtualId, "Solo puedes utilizar los siguientes parametros: staff/trade/friends/gifts/music/staffchat/console", 0, 34));
                    break;
            }
        }
    }
}

