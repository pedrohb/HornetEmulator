﻿using Plus.HabboHotel.Guides;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.HelperTool
{
    class InitHelperSessionChatMessageComposer : ServerPacket
    {
        public InitHelperSessionChatMessageComposer(GuideTour Tour)
            : base(ServerPacketHeader.InitHelperSessionChatMessageComposer)
        {
            //Help Requester
            base.WriteInteger(Tour.getNoob().Id); //UserID 1?
            base.WriteString(Tour.getNoob().Username); //UserName 1?
            base.WriteString(Tour.getNoob().Look); //Look 1?

            //Being helped by.
            base.WriteInteger(Tour.getHelper().Id); //UserID 2?
            base.WriteString(Tour.getHelper().Username); //UserName 2?
            base.WriteString(Tour.getHelper().Look); //Look 2?
        }
    }
}