﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.Communication.Packets.Outgoing.Groups;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class GetBadgeEditorPartsEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            Session.SendMessage(new BadgeEditorPartsComposer(
                MithServer.GetGame().GetGroupManager().Bases,
                MithServer.GetGame().GetGroupManager().Symbols,
                MithServer.GetGame().GetGroupManager().BaseColours,
                MithServer.GetGame().GetGroupManager().SymbolColours,
                MithServer.GetGame().GetGroupManager().BackGroundColours));
       
        }
    }
}
