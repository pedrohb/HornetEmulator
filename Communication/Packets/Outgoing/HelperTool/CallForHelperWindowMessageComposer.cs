﻿using Plus.HabboHotel.Guides;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.HelperTool
{
    class CallForHelperWindowMessageComposer : ServerPacket
    {
        public CallForHelperWindowMessageComposer(GuideTour Tour, bool Ishelper)
            : base(ServerPacketHeader.CallForHelperWindowMessageComposer)
        {
            if (Ishelper)
                Tour.getHelper().HelperPendingRequest = true;
            base.WriteBoolean(Ishelper); //IsHelper ?
            base.WriteInteger(1);       //? Tour type
            base.WriteString(Tour.getHelpRequest());    //? Instruction (Help message)
            base.WriteInteger(30); //avg time
        }
    }
}