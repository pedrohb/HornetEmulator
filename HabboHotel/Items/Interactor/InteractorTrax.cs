﻿using Plus.HabboHotel.GameClients;

namespace Plus.HabboHotel.Items.Interactor
{
    public class InteractorTrax : IFurniInteractor
    {
        public void BeforeRoomLoad(Item Item)
        {
        }
        public void OnPlace(GameClient Session, Item Item)
        {

        }

        public void OnRemove(GameClient Session, Item Item)
        {

        }

        public void OnTrigger(GameClient Session, Item Item, int Request, bool HasRights)
        {

        }

        public void OnWiredTrigger(Item Item)
        {

        }
    }
}