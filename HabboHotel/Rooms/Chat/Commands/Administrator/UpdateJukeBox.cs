﻿using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.Communication.Packets.Outgoing.Polls;
using Plus.HabboHotel.Rooms;
using Plus.Database.Interfaces;
using MySql.Data.MySqlClient;
using System;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;

using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Rooms.Chat.Commands;

namespace Plus.HabboHotel.Rewards.Rooms.Chat.Commands.Administrator
{
    class UpdateJukeBox : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_updatejukebox"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Añade una encuesta"; }
        }

        public void Execute(GameClients.GameClient Session, Room Room, string[] Params)
        {
            MithServer.GetGame().GetTraxSoundManager().Init();
            Session.SendWhisper("Se han actualizado con éxito las canciones del JukeBox.");

        }

    }
}
