﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Catalog.Frontpage
{
    public class Frontpage
    {
        public int _id;
        public string _frontName;
        public string _frontLink;
        public string _frontImage;

        public Frontpage(int Id, string FrontName, string FrontLink, string FrontImage)
        {
            this._id = Id;
            this._frontName = FrontName;
            this._frontLink = FrontLink;
            this._frontImage = FrontImage;
        }

        public int Id()
        {
            return this._id;
        }

        public string FrontName()
        {
            return this._frontName;
        }

        public string FrontLink()
        {
            return this._frontLink;
        }

        public string FrontImage()
        {
            return this._frontImage;
        }
    }
}
