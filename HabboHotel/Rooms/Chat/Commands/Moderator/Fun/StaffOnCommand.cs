﻿using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Database.Interfaces;
using Plus.HabboHotel.Users;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator.Fun
{
    class StaffOnCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_staffon"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Mira los staffs conectados."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            Dictionary<Habbo, UInt32> clients = new Dictionary<Habbo, UInt32>();

            foreach (var client in MithServer.GetGame().GetClientManager().GetClients.ToList())
            {
                if (client != null && client.GetHabbo() != null && client.GetHabbo().Rank > 2)
                    clients.Add(client.GetHabbo(), Convert.ToUInt16(client.GetHabbo().Rank));
            }
            StringBuilder Lista = new StringBuilder();
            DataTable Tabla = null;
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT count(users.id) FROM users WHERE users.online = '1' AND rank > 2");
                Tabla = dbClient.getTable();
            }
            foreach (DataRow Row in Tabla.Rows)
                Lista.Append("Hay " + Convert.ToString(Row[0]) + " staffs conectados:  \r\r");
            foreach (KeyValuePair<Habbo, UInt32> client in clients.OrderBy(key => key.Value))
            {
                Lista.Append("" + client.Key.GetClient().GetHabbo().Username + "(Rango: " + client.Key.GetClient().GetHabbo().Rank + ") (Sala Id:" + client.Key.GetClient().GetHabbo().CurrentRoomId +")\r");
            }
            Session.SendMessage(new MOTDNotificationComposer(Lista.ToString()));
            return;
        }
    }
}
