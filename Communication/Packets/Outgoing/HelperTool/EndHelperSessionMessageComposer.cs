﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.HelperTool
{
    class EndHelperSessionMessageComposer : ServerPacket
    {
        public EndHelperSessionMessageComposer(int i)
            : base(ServerPacketHeader.EndHelperSessionMessageComposer)
        {
            base.WriteInteger(i);
        }
    }
}