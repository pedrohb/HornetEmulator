﻿using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Items;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Rooms.TraxMachine;
using System;

namespace Plus.Communication.Packets.Incoming.TraxMachine
{
    internal class RemoveDiscFromPlayListEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            Room currentRoom = Session.GetHabbo().CurrentRoom;
            bool flag = !currentRoom.CheckRights(Session);
            if (!flag)
            {
                int num = Packet.PopInt();
                RoomTraxManager traxManager = currentRoom.GetTraxManager();
                bool flag2 = traxManager.Playlist.Count < num;
                if (!flag2)
                {
                    Item item = traxManager.Playlist[num];
                    bool flag3 = !traxManager.RemoveDisc(item);
                    if (!flag3)
                    {
                        return;
                    }
                }
                Session.SendNotification("No es posible remover el disco");
                //Session.SendMessage(new RoomNotificationComposer("", "Oops! Não foi possível remover o disco!", "error", "", "", true));
            }
        }
    }
}
