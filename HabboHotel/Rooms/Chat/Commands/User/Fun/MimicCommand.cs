﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Engine;
using Plus.Database.Interfaces;


namespace Plus.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class MimicCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_mimic"; }
        }

        public string Parameters
        {
            get { return "%username%"; }
        }

        public string Description
        {
            get { return "Te gusta el estilo de alguno? Copialo!"; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Por favor introuce el nombre de usuario que quieres copiar.");
                return;
            }

            GameClient TargetClient = MithServer.GetGame().GetClientManager().GetClientByUsername(Params[1]);
            if (TargetClient.GetHabbo() == null)
            {
                Session.SendWhisper("Este usuario no existe o no esta conectado");
                return;
            }
            /*
            if (!TargetClient.GetHabbo().AllowMimic)
            {
                Session.SendWhisper("Oops, you cannot mimic this user - sorry!");
                return;
            }*/

            Session.GetHabbo().Gender = TargetClient.GetHabbo().Gender;
            Session.GetHabbo().Look = TargetClient.GetHabbo().Look;

            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("UPDATE `users` SET `gender` = @gender, `look` = @look WHERE `id` = @id LIMIT 1");
                dbClient.AddParameter("gender", Session.GetHabbo().Gender);
                dbClient.AddParameter("look", Session.GetHabbo().Look);
                dbClient.AddParameter("id", Session.GetHabbo().Id);
                dbClient.RunQuery();
            }

            RoomUser User = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (User != null)
            {
                Session.SendMessage(new UserChangeComposer(User, true));
                Session.SendMessage(new AvatarAspectUpdateMessageComposer(TargetClient.GetHabbo().Look, TargetClient.GetHabbo().Gender));
                Room.SendMessage(new UserChangeComposer(User, false));
            }
        }
    }
}