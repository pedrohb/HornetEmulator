﻿using Plus.HabboHotel.Guides;
using Plus.HabboHotel.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.HelperTool
{
    class HelperSessionVisiteRoomMessageComposer : ServerPacket
    {
        public HelperSessionVisiteRoomMessageComposer(Room room)
            : base(ServerPacketHeader.HelperSessionVisiteRoomMessageComposer)
        {
            base.WriteInteger(room != null ? room.Id : 0);
        }
    }
}