﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.HelperTool;
using Plus.HabboHotel.Guides;
using Plus.Database.Interfaces;
using System.Data;
using Plus.Communication.Packets.Outgoing.Messenger;

namespace Plus.Communication.Packets.Incoming.HelperTool
{
    class AcceptHelperSessionEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            GuideTour tour = MithServer.GetGame().GetGuideManager().getGuideTourByHelper(Session.GetHabbo());

            if (tour == null)
            {
                return;
            }

            bool accepted = Packet.PopBoolean();

            if (!accepted)
            {
                MithServer.GetGame().GetGuideManager().declineTour(tour);
            }
            else
            {
                MithServer.GetGame().GetGuideManager().startSession(tour, Session.GetHabbo());
                DataRow Row = null;
                using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
                {
                    dbClient.RunQuery("SELECT id FROM lince_helpalert WHERE id_user = '" + tour.getNoob().Id + "' AND lince_id = 0 ORDER BY id DESC LIMIT 1");
                    Row = dbClient.getRow();
                    dbClient.RunQuery("UPDATE `lince_helpalert` SET `lince_id` = '" + Session.GetHabbo().Id + "' WHERE `id` = '" + Convert.ToInt32(Row[0]) + "' LIMIT 1");
                    MithServer.GetGame().GetClientManager().StaffAlert(new NewConsoleMessageComposer(0x7fffffff, "ATENCIÓN: El usuario: " + Session.GetHabbo().Username + " ha registrado el reporte: " + Row[0]), Session.GetHabbo().Id);
                }
            }


        }
    }
}
