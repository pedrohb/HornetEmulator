﻿using Plus.Communication.Packets.Outgoing.Groups;
using Plus.HabboHotel.GameClients;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.Groups
{
    class GetThreadDataEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            var ForumId = Packet.PopInt(); 
            var ThreadId = Packet.PopInt();
            var StartIndex = Packet.PopInt(); 
            var length = Packet.PopInt(); 

            var Forum = MithServer.GetGame().GetGroupForumManager().GetForum(ForumId);

            if (Forum == null)
            {
                return;
            }

            var Thread = Forum.GetThread(ThreadId);
            if (Thread == null)
            {
                return;
            }

            
            Session.SendMessage(new ThreadDataComposer(Thread, StartIndex, length));
            
        }
    }
}
