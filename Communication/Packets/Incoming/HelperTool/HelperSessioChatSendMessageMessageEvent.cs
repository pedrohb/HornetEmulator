﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;
using Plus.Communication.Packets.Outgoing.HelperTool;
using Plus.HabboHotel.Guides;
using Plus.Communication.Packets.Outgoing;

namespace Plus.Communication.Packets.Incoming.HelperTool
{
    class HelperSessioChatSendMessageMessageEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            GuideTour tour = MithServer.GetGame().GetGuideManager().getGuideTourByHabbo(Session.GetHabbo());

            if (tour != null)
            {
                GuideChatMessage chatMessage = new GuideChatMessage(Session.GetHabbo().Id, Packet.PopString(), Convert.ToInt32(MithServer.GetUnixTimestamp()));
                tour.addMessage(chatMessage);
                ServerPacket serverMessage = new HelperSessionSendChatMessageComposer(chatMessage);
                tour.getHelper().GetClient().SendMessage(serverMessage);
                tour.getNoob().GetClient().SendMessage(serverMessage);
            }

        }
    }
}