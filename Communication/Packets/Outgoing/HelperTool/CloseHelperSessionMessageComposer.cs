﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.HelperTool
{
    class CloseHelperSessionMessageComposer : ServerPacket
    {
        public CloseHelperSessionMessageComposer()
            : base(ServerPacketHeader.CloseHelperSessionMessageComposer)
        {

        }
    }
}