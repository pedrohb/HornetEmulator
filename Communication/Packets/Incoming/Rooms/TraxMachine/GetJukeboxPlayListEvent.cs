﻿using Plus.Communication.Packets.Outgoing.Sound;
using Plus.Communication.Packets.Outgoing.TraxMachine;
using Plus.HabboHotel.GameClients;
using System;

namespace Plus.Communication.Packets.Incoming.TraxMachine
{
    internal class GetJukeboxPlayListEvent : IPacketEvent
    {
        public void Parse(GameClient Session, ClientPacket Packet)
        {
            bool flag = Session.GetHabbo().CurrentRoom != null;
            if (flag)
            {
                Session.SendMessage(new SetJukeboxPlayListComposer(Session.GetHabbo().CurrentRoom));
            }
        }
    }
}
