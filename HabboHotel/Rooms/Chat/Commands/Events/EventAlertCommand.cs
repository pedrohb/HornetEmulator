﻿using Plus.Communication.Packets.Outgoing.Rooms.Notifications;
using Plus.HabboHotel.GameClients;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Events
{
    internal class EventAlertCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get
            {
                return "command_event_alert";
            }
        }
        public string Parameters
        {
            get
            {
                return "%message%";
            }
        }
        public string Description
        {
            get
            {
                return "Enviar una alerta al hotel sobre el evento";
            }
        }
        public void Execute(GameClient Session, Room Room, string[] Params)
        {
            if (Session != null)
            {
                if (Room != null)
                {
                    if (Params.Length == 1)
                    {
                        Session.SendWhisper("Por favor, escriba el nombre del evento.");
                        return;
                    }
                    else
                    {
                        string Message = CommandManager.MergeParams(Params, 1);
                        MithServer.GetGame().GetClientManager().SendMessage(new RoomNotificationComposer("¡NUEVO EVENTO!", "\n<font color=\"#FF0000\">¡Un nuevo evento te esta esperando ahora mismo!</font>\n\n" + "Gana Diamantes o algún Rare Clase C participando en uno de nuestros eventos especiales.\n\n" + "Si quieres participar, Dale clic al botón de la parte inferior (Ir al evento).\n\n" + "<font color=\"#FF0000\">¿Qué esperas para ir? ¡Se hace tarde!</font>\n\n", "events", "¡Ir al evento!", "event:navigator/goto/" + Session.GetHabbo().CurrentRoomId));
                    }
                }
            }
        }
    }
}