﻿using System;
using Plus.Communication.Packets.Outgoing;
using Plus.HabboHotel.Talentos;

namespace Plus.Communication.Packets.Incoming.Talents
{
    class GetTalentTrackEvent : IPacketEvent
    {
        public void Parse(HabboHotel.GameClients.GameClient Session, ClientPacket Packet)
        {
            string Type = Packet.PopString();

            var talentos = MithServer.GetGame().GetTalentoManager().GetTalents(Type, -1);
            var fail = -1;
            if (talentos == null)
                return;

            ServerPacket Root = new ServerPacket(ServerPacketHeader.TalentTrackMessageComposer);
            Root.WriteString(Type);
            Root.WriteInteger(talentos.Count);
            foreach (var habbi in talentos)
            {
                Root.WriteInteger(habbi.Level);
                int nm = (fail == -1) ? 1 : 0;
                Root.WriteInteger(nm);
                var talents2 = MithServer.GetGame().GetTalentoManager().GetTalents(Type, habbi.Id);
                Root.WriteInteger(talents2.Count);
                foreach (var habbi2 in talents2)
                {
                    if (habbi2.GetAchievement() == null)
                        throw new NullReferenceException(
                            string.Format("The following talent achievement can't be found: {0}",
                                habbi2.AchievementGroup));

                    int num = (fail != -1 && fail < habbi2.Level)
                        ? 0
                        : (Session.GetHabbo().GetAchievementData(habbi2.AchievementGroup) == null)
                            ? 1
                            : (Session.GetHabbo().GetAchievementData(habbi2.AchievementGroup).Level >=
                               habbi2.AchievementLevel)
                                ? 2
                                : 1;
                    Root.WriteInteger(habbi2.GetAchievement().Id);
                    Root.WriteInteger(0);
                    Root.WriteString(string.Format("{0}{1}", habbi2.AchievementGroup, habbi2.AchievementLevel));
                    Root.WriteInteger(num);
                    Root.WriteInteger((Session.GetHabbo().GetAchievementData(habbi2.AchievementGroup) != null)
                        ? Session.GetHabbo().GetAchievementData(habbi2.AchievementGroup).Progress
                        : 0);
                    Root.WriteInteger((habbi2.GetAchievement() == null)
                        ? 0
                        : habbi2.GetAchievement().Levels[habbi2.AchievementLevel].Requirement);
                    if (num != 2 && fail == -1)
                        fail = habbi2.Level;
                }
                Root.WriteInteger(0);
                if (habbi.Type == "citizenship" && habbi.Level == 4)
                {
                    Root.WriteInteger(2);
                    Root.WriteString("HABBO_CLUB_VIP_7_DAYS");
                    Root.WriteInteger(7);
                    Root.WriteString(habbi.Prize);
                    Root.WriteInteger(0);
                }
                else
                {
                    Root.WriteInteger(1);
                    Root.WriteString(habbi.Prize);
                    Root.WriteInteger(0);
                }
            }
            Session.SendMessage(Root);



        }



    }
}







