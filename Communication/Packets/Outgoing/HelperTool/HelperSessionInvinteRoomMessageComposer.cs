﻿using Plus.HabboHotel.Guides;
using Plus.HabboHotel.Rooms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.HelperTool
{
    class HelperSessionInvinteRoomMessageComposer : ServerPacket
    {
        public HelperSessionInvinteRoomMessageComposer(Room room)
            : base(ServerPacketHeader.HelperSessionInvinteRoomMessageComposer)
        {
            base.WriteInteger(room != null ? room.Id : 0);
            base.WriteString(room != null ? room.Name : "");
        }
    }
}