﻿using Plus.HabboHotel.Items;
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Rooms.TraxMachine;
using System;

namespace Plus.Communication.Packets.Outgoing.TraxMachine
{
    internal class SetJukeboxNowPlayingComposer : ServerPacket
    {
        public SetJukeboxNowPlayingComposer(Room room) : base(ServerPacketHeader.SetJukeboxNowPlayingMessageComposer)
        {
            RoomTraxManager traxManager = room.GetTraxManager();
            if ((!traxManager.IsPlaying ? true : (object)traxManager.ActualSongData == (object)null))
            {
                base.WriteInteger(-1);
                base.WriteInteger(-1);
                base.WriteInteger(-1);
                base.WriteInteger(-1);
                base.WriteInteger(-1);
            }
            else
            {
                Item actualSongData = traxManager.ActualSongData;
                TraxMusicData musicByItem = traxManager.GetMusicByItem(actualSongData);
                int musicIndex = traxManager.GetMusicIndex(actualSongData);
                base.WriteInteger(musicByItem.Id);
                base.WriteInteger(musicIndex);
                base.WriteInteger(musicByItem.Id);
                base.WriteInteger(checked((int)((double)traxManager.TotalPlayListLength * 1000)));
                base.WriteInteger(checked((int)((double)traxManager.ActualSongTimePassed * 1000)));
            }
        }
    }
}
