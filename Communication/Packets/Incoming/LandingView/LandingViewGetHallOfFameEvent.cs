﻿using Plus.Communication.Interfaces;
using Plus.Communication.Packets.Outgoing;
using Plus.Database.Interfaces;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Incoming.LandingView
{
    class GetHallOfFame
    {
        private static GetHallOfFame instance = new GetHallOfFame();
        internal static GetHallOfFame getInstance()
        {
            return instance;
        }
        private List<UserCompetition> usersWithRank;

        internal GetHallOfFame()
        {
            usersWithRank = new List<UserCompetition>();
        }

        internal void Load()
        {
            using (IQueryAdapter dbQuery = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                usersWithRank = new List<UserCompetition>();

                dbQuery.SetQuery("SELECT users.id,users.username,users.look,user_stats.AchievementScore FROM users INNER JOIN user_stats ON user_stats.id = users.id WHERE users.rank = 1 ORDER BY user_stats.AchievementScore DESC LIMIT 16");
                DataTable gUsersTable = dbQuery.getTable();

                foreach (DataRow Row in gUsersTable.Rows)
                {
                    var staff = new UserCompetition(Row);
                    usersWithRank.Add(staff);
                }
            }

        }

        internal void Serialize(ServerPacket message)
        {
            message.WriteInteger(usersWithRank.Count);
            foreach (UserCompetition user in usersWithRank)
            {
                message.WriteInteger(user.userId); //userID
                message.WriteString(user.userName);//userName
                message.WriteString(user.Look);//Look
                message.WriteInteger((int)user.score); //rank
                message.WriteInteger((int)user.score);//score
            }
        }
    }

    class UserCompetition
    {
        internal int userId, score;
        internal string userName, Look;

        internal UserCompetition(DataRow row)
        {
            this.userId = (int)row[0];
            this.userName = (string)row[1];
            this.Look = (string)row[2];
            this.score = Convert.ToInt32(row[3].ToString());
        }
    }
}
