﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.HabboHotel.Guides
{
    public class GuideFindNewHelper
    {
        public GuideTour tour;
        public int checkSum;

        public GuideFindNewHelper(GuideTour tour)
        {
            this.tour = tour;
            this.checkSum = tour.checkSum;
        }


        public void run()
        {
            if (!this.tour.isEnded() && this.tour.checkSum == this.checkSum && this.tour.getHelper() == null)
            {
                MithServer.GetGame().GetGuideManager().findHelper(this.tour); //aca
            }
        }
    }
}
