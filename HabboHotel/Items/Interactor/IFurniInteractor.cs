﻿
using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Items;
using System;
using System.Drawing;
using System.Linq;
using System.Runtime.CompilerServices;
namespace Plus.HabboHotel.Items.Interactor
{
    public interface IFurniInteractor
    {

        void OnPlace(GameClient Session, Item Item);
        void OnRemove(GameClient Session, Item Item);
        void OnTrigger(GameClient Session, Item Item, int Request, bool HasRights);
        void OnWiredTrigger(Item Item);
        void BeforeRoomLoad(Item Item);
       
       

    }
}