﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Plus.HabboHotel.Users;
using Plus.HabboHotel.GameClients;

using Plus.Database.Interfaces;
using Plus.Communication.Packets.Outgoing.Notifications;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class LastEvents : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_lastevents"; }
        }

        public string Parameters
        {
            get { return "(username)"; }
        }

        public string Description
        {
            get { return "Muestra toda la información de un usuario."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            DataTable Tabla = null;
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT users.username, events_logs.event,TIMESTAMPDIFF(MINUTE, events_logs.fecha, now()) FROM events_logs INNER JOIN users ON users.id = events_logs.user_id ORDER BY events_logs.id DESC LIMIT 10");
                Tabla = dbClient.getTable();
            }
            StringBuilder Lista = new StringBuilder();
            Lista.Append("Los últimos 10 eventos abiertos son (Orden Descendiente): \r\r");
            foreach (DataRow Row in Tabla.Rows)
            {
                Lista.Append("Usuario: " + Convert.ToString(Row[0]) + " - Evento Abierto: " + Convert.ToString(Row[1]) + " (Hace: "+Convert.ToString(Row[2])+" minutos) \r\r");
            }
            Session.SendMessage(new MOTDNotificationComposer(Lista.ToString()));
            return;
        }
    }
}