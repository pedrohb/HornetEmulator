﻿using log4net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Plus.Database.Interfaces;
using System.Data;
using Plus.Communication.Packets.Outgoing.Notifications;
using Plus.Communication.Packets.Outgoing.TargetOffers;
using Plus.HabboHotel.GameClients;
namespace Plus.HabboHotel.TargetOffers
{
    public class TargetOffersManager
    {
        private static readonly ILog log = LogManager.GetLogger("Plus.HabboHotel.TargetOffers");
        GameClients.GameClient Session;
        public int _offerid { get; set; }
        public string _itemName { get; set; }
        public string _offerTitle { get; set; }
        public int _itemId { get; set; }
        public string _offerDescription { get; set; }
        public int _offerCreditsPrice { get; set; }
        public int _offerDiamondsPrice { get; set; }
        private int _offerTime { get; set; }
        private int _offerMaxBuy { get; set; }
        private string _offerBadgeReward { get; set; }
        private string _offerIconImage { get; set; }
        private string _offerImage { get; set; }
        private bool _offerIsEnabled = true;


        public TargetOffersManager()
        {
            this.LoadTargetOffersData();
            log.Info("Target Offers Manager -> LOADED");
        }
        public void TargetOffers(int OfferId, string ItemName, int ItemId, string OfferTitle, string OfferDescription, int OfferCreditsPrice, int OfferDiamondsPrice, int OfferTime, int OfferMaxBuy, string OfferBadgeReward, string OfferIconImage, string OfferImage, bool OfferIsEnabled)
        {
            this._offerid = OfferId;
            this._itemName = ItemName;
            this._itemId = ItemId;
            this._offerTitle = OfferTitle;
            this._offerDescription = OfferDescription;
            this._offerCreditsPrice = OfferCreditsPrice;
            this._offerDiamondsPrice = OfferDiamondsPrice;
            this._offerTime = OfferTime;
            this._offerMaxBuy = OfferMaxBuy;
            this._offerBadgeReward = OfferBadgeReward;
            this._offerIconImage = OfferIconImage;
            this._offerImage = OfferImage;
            this._offerIsEnabled = OfferIsEnabled;
        }
        public bool LoadTargetOffersData()
        {
            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT id,ItemName,ItemId,OfferTitle,OfferDescription,OfferCreditsPrice,OfferDiamondPrice,OfferTime,OfferMaxBuy,OfferBadgeReward,OfferIconImage,OfferImage,OfferIsEnabled FROM targetoffer WHERE OfferIsEnabled = 1 ORDER BY id DESC LIMIT 1");
                DataRow Row = dbClient.getRow();
                if (Row == null)
                {
                    this.TargetOffers(0, null, 0, null, null, 0, 0, 0, 0, null, null, null, false);
                    return false;
                }
                this.TargetOffers(Convert.ToInt32(Row["id"]), Convert.ToString(Row["ItemName"]), Convert.ToInt32(Row["ItemId"]), Convert.ToString(Row["OfferTitle"]), Convert.ToString(Row["OfferDescription"]), Convert.ToInt32(Row["OfferCreditsPrice"]), Convert.ToInt32(Row["OfferDiamondPrice"]), Convert.ToInt32(Row["OfferTime"]), Convert.ToInt32(Row["OfferMaxBuy"]), Convert.ToString(Row["OfferBadgeReward"]), Convert.ToString(Row["OfferIconImage"]), Convert.ToString(Row["OfferImage"]), Convert.ToBoolean(Row["OfferIsEnabled"]));
                return true;
            }
        }
        public void AddLogUserOffer(int UserID, int OfferID)
        {
            using (var dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.RunQuery("INSERT INTO `user_offers` (`user_id`, `offer_id`) VALUES ('" + UserID + "', '" + OfferID + "')");
            }
        }
        public bool UserCanBuy(GameClient Session)
        {
            if (Session.GetHabbo().CountBuyOffer() < this.OfferMaxBuy())
                return true;
            else
                return false;
        }
        public bool OfferIsEnable()
        {
            return this._offerIsEnabled;
        }
        public string ItemName()
        {
            return this._itemName;
        }
        public int OfferID()
        {
            return this._offerid;
        }
        public int ItemId()
        {
            return this._itemId;
        }
        public string OfferTitle()
        {
            return this._offerTitle;
        }
        public string OfferDescription()
        {
            return this._offerDescription;
        }
        public int OfferCreditsPrice()
        {
            return this._offerCreditsPrice;
        }
        public int OfferDiamondsPrice()
        {
            return this._offerDiamondsPrice;
        }
        public int OfferTime()
        {
            return this._offerTime;
        }
        public int OfferMaxBuy()
        {
            return this._offerMaxBuy;
        }
        public string OfferBadgeReward()
        {
            return this._offerBadgeReward;
        }
        public string OfferIconImage()
        {
            return this._offerIconImage;
        }
        public string OfferImage()
        {
            return this._offerImage;
        }
    }
}