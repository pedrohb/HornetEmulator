﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Pathfinding;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class SelfieCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_selfie"; }
        }

        public string Parameters
        {
            get { return ""; }
        }

        public string Description
        {
            get { return "Tomate una foto"; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {

            RoomUser ThisUser = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (ThisUser == null)
                return;
            Room.SendMessage(new ChatComposer(ThisUser.VirtualId, "*Saca su iPhone*", 0, ThisUser.LastBubble));
            Session.GetHabbo().Effects().ApplyEffect(65);
            Room.SendMessage(new ChatComposer(ThisUser.VirtualId, "*Se toma una Selfie*", 0, ThisUser.LastBubble));
        }
    }
}
