﻿using Plus.HabboHotel.Guides;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.HelperTool
{
    class HelperSessionSendChatMessageComposer : ServerPacket
    {
        public HelperSessionSendChatMessageComposer(GuideChatMessage Mensaje)
            : base(ServerPacketHeader.HelperSessionSendChatMessageComposer)
        {
            base.WriteString(Mensaje.message); //Message
            base.WriteInteger(Mensaje.userId);   //Sender ID
        }
    }
}