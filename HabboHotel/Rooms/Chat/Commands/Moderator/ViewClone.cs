﻿using System;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections.Generic;

using Plus.HabboHotel.Users;
using Plus.HabboHotel.GameClients;

using Plus.Database.Interfaces;
using Plus.Communication.Packets.Outgoing.Notifications;

namespace Plus.HabboHotel.Rooms.Chat.Commands.Moderator
{
    class ViewCloneCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_view_clone"; }
        }

        public string Parameters
        {
            get { return "(username)"; }
        }

        public string Description
        {
            get { return "Muestra toda la información de un usuario."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {
            if (Params.Length == 1)
            {
                Session.SendWhisper("Please enter the username of the user you wish to view.");
                return;
            }
            DataTable Tabla = null;
            DataRow UserData = null;
            string Username = Params[1];

            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`username`,`ip_last`,`mail`,`rank`,`motto`,`credits`,`activity_points`,`vip_points`,`gotw_points`,`online`,`rank_vip` FROM users WHERE `username` = @Username LIMIT 1");
                dbClient.AddParameter("Username", Username);
                UserData = dbClient.getRow();
            }


            if (UserData == null)
            {
                Session.SendNotification("El nombre (" + Username + ") no existe");
                return;
            }

            using (IQueryAdapter dbClient = MithServer.GetDatabaseManager().GetQueryReactor())
            {
                dbClient.SetQuery("SELECT `id`,`username`,`ip_last` FROM users WHERE `ip_last` = (SELECT `ip_last` FROM users WHERE `username` = @Username)");
                dbClient.AddParameter("Username", Username);
                Tabla = dbClient.getTable();
            }

            GameClient TargetClient = MithServer.GetGame().GetClientManager().GetClientByUsername(Username);
            StringBuilder HabboInfo = new StringBuilder();
            HabboInfo.Append(Convert.ToString("Username: " + UserData["username"] + " Ip: " + UserData["ip_last"]) + "\r\r");
            HabboInfo.Append("Clones encontrados: " + Tabla.Rows.Count + "\r\r");
            foreach (DataRow Row in Tabla.Rows)
            {
                HabboInfo.Append("Id : " + Convert.ToInt32(Row[0]) + " - " + "Usuario: " + Convert.ToString(Row[1]) + "\r\r");
            }
            Session.SendMessage(new MOTDNotificationComposer(HabboInfo.ToString()));
        }
    }
}
