﻿using Plus.HabboHotel.GameClients;
using Plus.HabboHotel.Groups.Forums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Plus.Communication.Packets.Outgoing.Groups
{
    class ForumsListDataComposer : ServerPacket
    {
        public ForumsListDataComposer(ICollection<GroupForum> Forums, GameClient Session, int ViewOrder = 0, int StartIndex = 0, int MaxLength = 20)
            : base(ServerPacketHeader.ForumsListDataMessageComposer)
        {
            base.WriteInteger(ViewOrder);
            base.WriteInteger(StartIndex);
            base.WriteInteger(StartIndex);

            base.WriteInteger(Forums.Count); // Liste Compte Forums

            foreach (var Forum in Forums)
            {
                var lastpost = Forum.GetLastPost();
                var isn = lastpost == null;
                base.WriteInteger(Forum.Id); // Peux-être ID?
                base.WriteString(Forum.Name); // Nom du Forum
                base.WriteString(Forum.Description); // Je sais pas..
                base.WriteString(Forum.Group.Badge); // Badge du groupe
                base.WriteInteger(0);// Je sais pas..
                base.WriteInteger(0);// Score
                base.WriteInteger(Forum.MessagesCount);//Compte Message
                base.WriteInteger(Forum.UnreadMessages(Session.GetHabbo().Id));// Message non lus
                base.WriteInteger(0);// Je sais pas..
                base.WriteInteger(!isn ? lastpost.GetAuthor().Id: 0);// Dernier ID message lu
                base.WriteString(!isn ? lastpost.GetAuthor().Username : ""); // Dernier Utilisateur message lu
                base.WriteInteger(!isn ? (int)PowerBird.GetUnixTimestamp() - lastpost.Timestamp : 0); // dernier message lu avec le temps.
            }
        }
    }
}
