﻿using System;
using System.Linq;
using System.Text;
using System.Collections.Generic;

using Plus.HabboHotel.Rooms;
using Plus.HabboHotel.Pathfinding;
using Plus.HabboHotel.GameClients;
using Plus.Communication.Packets.Outgoing.Rooms.Chat;

namespace Plus.HabboHotel.Rooms.Chat.Commands.User.Fun
{
    class SmokeCommand : IChatCommand
    {
        public string PermissionRequired
        {
            get { return "command_smoke"; }
        }

        public string Parameters
        {
            get { return "(target)"; }
        }

        public string Description
        {
            get { return "Empuja a otro usuario."; }
        }

        public void Execute(GameClients.GameClient Session, Rooms.Room Room, string[] Params)
        {


            RoomUser ThisUser = Room.GetRoomUserManager().GetRoomUserByHabbo(Session.GetHabbo().Id);
            if (ThisUser == null)
                return;

            Room.SendMessage(new ChatComposer(ThisUser.VirtualId, "*Saca su porrito*", 0, ThisUser.LastBubble));
            Room.SendMessage(new ChatComposer(ThisUser.VirtualId, "*Lo enciende*", 0, ThisUser.LastBubble));
            Room.SendMessage(new ChatComposer(ThisUser.VirtualId, "*Smoke weed everyday*", 0, ThisUser.LastBubble));

            ThisUser.ApplyEffect(508);
        }
    }
}
